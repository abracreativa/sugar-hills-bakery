<?php

/**
 * Fired during plugin activation
 *
 * @link       http://ocredg.com
 * @since      1.0.0
 *
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/includes
 * @author     Nahuel José <nahueljose@gmail.com>
 */
class Sugar_Hills_Booking_Calendar_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
