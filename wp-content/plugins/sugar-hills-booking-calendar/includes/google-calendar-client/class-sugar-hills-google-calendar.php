<?php
require_once __DIR__ . '/vendor/autoload.php';
/**
 * 
 * Class to handle Google Calendar
 *
 * @since      1.0.0
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/includes
 * @author     Nahuel José <nahueljose@gmail.com>
 */
class Sugar_Hills_Google_Calendar {

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The unique identifier of the WP option to store / get access token
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $option_name
	 */
	protected $option_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * The current Calendar ID
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $calendar_id    The current Calendar ID
	 */
	protected $calendar_id;

	/**
	 * The current Google client
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Google_Client    $client    The current Google client
	 */
    protected $client;
    
    /**
    * The template for time availability
    *
    * @since    1.0.0
    * @access   protected
    * @var      array    $daily_availability_template    The template
    */
   protected $daily_availability_template;

	public function __construct( $plugin_name, $version ) {
        // Basic options
        $this->version = $version;
        $this->plugin_name = $plugin_name;
        $this->option_name = $plugin_name . '_google_access_token';
        
        // Set calendar ID
		$options = get_option($this->plugin_name . '_settings');
        $calendar_id =  isset($options[$this->plugin_name . '-google-calendar-id']) &&
                        trim(strlen($options[$this->plugin_name . '-google-calendar-id'])) > 0 ?
                        trim($options[$this->plugin_name . '-google-calendar-id']) : 'primary';
        $this->calendar_id = $calendar_id;
        
        // Init the client
        $this->client = new Google_Client();
        $this->client->setApplicationName('Sugar Hills BYT');
        $this->client->setScopes(Google_Service_Calendar::CALENDAR);
        $this->client->setAuthConfig(__DIR__ . '/client_secret.json');
        $this->client->setAccessType('offline');

        /**
         * Construct the availability template *manually*
         * 0-indexed week days
         * Times in 24hs format
         * true = available (Default)
         */
        $this->daily_availability_template = array(
            // Sundays
            array(),
            // Mondays
            array(12 => true, 13 => true, 14 => true, 15 => true, 16 => true),
            // Tuesdays
            array(12 => true, 13 => true, 14 => true, 15 => true, 16 => true),
            // Wednesdays
            array(12 => true, 13 => true, 14 => true, 15 => true, 16 => true),
            // Thursdays
            array(12 => true, 13 => true, 14 => true, 15 => true, 16 => true),
            // Fridays
            array(12 => true, 13 => true, 14 => true, 15 => true, 16 => true),
            // Saturdays
            array(9 => true, 10 => true, 11 => true, 12 => true, 13 => true, 14 => true)
        );
    }
    
    /**
     * Build the monthly availabily template
     * @return array
     */
    public function buildMonthlyAvailabilityTemplate( $date ){
        /**
         * DO NOT use $this->date here.
         * Here we absolutely want the time at UTC
         */
        $number_of_days = intval(date('t', strtotime($date)));
        $month_template = date('Y-m-%02\d', strtotime($date));
        $monthly_availability = array();

        for ($i=1; $i <= $number_of_days; $i++) {
            $current_date = date('w', strtotime(sprintf($month_template, $i)));
            $current_template = $this->daily_availability_template[intval($current_date)];
            $monthly_availability[$i] = $current_template;
        }

        return $monthly_availability;
    }

    /**
     * Init the class
     * @return Google_Client
     */
    public function init(){
        // Get the access token
        $accessToken = $this->getAccessToken();
        // Set the access token
        $this->client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($this->client->isAccessTokenExpired()) {
            $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
            // Save the new access token
            $this->saveAccessToken($this->client->getAccessToken());
        }

        // Returns the authorized client
        return $this->client;
    }

    /**
     * Validates a given date
     * @return Bool
     */
    public function validateDate($date)
    {
        $test_arr  = explode('-', $date);
        if (count($test_arr) == 3) {
            if (checkdate($test_arr[1], $test_arr[2], $test_arr[0])) {
                // valid date ...
                return true;
            } else {
                // problem with date ...
                return false;
            }
        } else {
            // problem with input ...
            return false;
        }
    }

    /**
     * Returns an URL to get auth code
     * @return String the URL
     */
    public function createAuthUrl(){
        return $this->client->createAuthUrl();
    }

    /**
     * Returns an access token from an authCode
     * @return String the access token
     */
    public function fetchAccessTokenWithAuthCode( $authCode ){
        $accessToken = $this->client->fetchAccessTokenWithAuthCode($authCode);
        return $this->saveAccessToken($accessToken);
    }

    /**
     * Returns an access token if possible
     * @return String the access token
     */
    public function getAccessToken(){
        return json_decode(get_option($this->option_name, false), true);
    }

    /**
     * Save an access token
     * @return String the access token
     */
    public function saveAccessToken( $accessToken ){
        update_option($this->option_name, json_encode($accessToken));
        return $accessToken;
    }

    /**
     * Get a date in current WP timezone
     * @return String the access token
     */
    public function date( $format, $timestr = false ){
        $hour_in_seconds = 60 * 60;
        return ( $timestr ) ? date( $format, $timestr + ( get_option( 'gmt_offset' ) * $hour_in_seconds ) ) :
                              date( $format, time() + ( get_option( 'gmt_offset' ) * $hour_in_seconds ) );
    }

    /**
     * Get the monthly availability of the calendar
     * @return Array the availability
     */
    public function getMonthlyAvailability( $query_date = false ){
        // Get the day. Else, use today
        if( !$this->validateDate($query_date) ){
            $query_date = $this->date('Y-m-d');
        }

        // First day of the month.
        $mFirstDay = $this->date('Y-m-d', strtotime($query_date . ' -31 days'));
        // Last day of the month.
        $mLastDay = $this->date('Y-m-d', strtotime($query_date . ' +31 days'));
        // Setup the interval
        $timeMin = $this->date('c', strtotime($mFirstDay));
        $timeMax = $this->date('c', strtotime($mLastDay));

        // Get the API client and construct the service object.
        $service = new Google_Service_Calendar($this->client);

        // Print the next events on the user's calendar.;
        $optParams = array(
        'maxResults' => 999,
        'orderBy' => 'startTime',
        'singleEvents' => true,
        'timeMin' => $timeMin,
        'timeMax' => $timeMax,
        );
        $results = $service->events->listEvents($this->calendar_id, $optParams);

        // Default output
        $output = [
            'timeMin' => $timeMin,
            'timeMax' => $timeMax,
            'availability' => $this->buildMonthlyAvailabilityTemplate( $query_date )
        ];

        // Parse results
        if (!empty($results->getItems())) {
            foreach ($results->getItems() as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                $end = $event->end->dateTime;
                if (empty($end)) {
                    $end = $event->end->date;
                }

                $day_of_month = intval($this->date('j', strtotime($start)));
                $start_hour   = intval($this->date('G', strtotime($start)));
                $end_hour     = intval($this->date('G', strtotime($end)));

                foreach( $output['availability'][$day_of_month] as $t_time => $t_available ){
                    // Mark the current date in the template
                    // as UNAVAILABLE
                    if( $t_time >= $start_hour && $t_time < $end_hour ){
                        $output['availability'][$day_of_month][$t_time] = false;
                    }
                }
            }
        }

        return $output;
    }

    /**
     * Add event to Google Calendar
     * @return {String} link to event
     */
    public function add_event($event_details){
        // Get the API client and construct the service object.
        $service = new Google_Service_Calendar($this->client);
        // Get the event class
        $event   = new Google_Service_Calendar_Event($event_details);        
        // Insert event
        $event   = $service->events->insert($this->calendar_id, $event, array('sendNotifications' => true));
        // Return link
        return $event->htmlLink;
    }
}
