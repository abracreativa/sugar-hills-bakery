<?php
require_once __DIR__ . '/functions.php';

// Get the day. Else, use today
$query_date = @$_GET['date'];
if( !validateDate($query_date) ){
    $query_date = date('Y-m-d');
}

// First day of the month.
$mFirstDay = date('Y-m-01', strtotime($query_date));
// Last day of the month.
$mLastDay = date('Y-m-t', strtotime($query_date));
// Setup the interval
$timeMin = date('c', strtotime($mFirstDay));
$timeMax = date('c', strtotime($mLastDay));

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Calendar($client);

// Print the next 10 events on the user's calendar.
$calendarId = 'primary';
$optParams = array(
  'maxResults' => 10,
  'orderBy' => 'startTime',
  'singleEvents' => true,
  'timeMin' => $timeMin,
  'timeMax' => $timeMax,
);
$results = $service->events->listEvents($calendarId, $optParams);

// Default output
$output = [
    'timeMin' => $timeMin,
    'timeMax' => $timeMax,
    'events' => []
];

// Parse results
if (!empty($results->getItems())) {
    foreach ($results->getItems() as $event) {
        $start = $event->start->dateTime;
        if (empty($start)) {
            $start = $event->start->date;
        }
        $output['events'][] = [
            'dateTime' => $start,
            'summary' => $event->getSummary()
        ];
    }
}

// Output the JSON encoded results
echo json_encode($output);