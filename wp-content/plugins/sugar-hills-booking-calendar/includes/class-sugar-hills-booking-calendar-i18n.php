<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://ocredg.com
 * @since      1.0.0
 *
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/includes
 * @author     Nahuel José <nahueljose@gmail.com>
 */
class Sugar_Hills_Booking_Calendar_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'sugar-hills-booking-calendar',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
