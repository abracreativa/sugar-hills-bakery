<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://ocredg.com
 * @since             1.0.0
 * @package           Sugar_Hills_Booking_Calendar
 *
 * @wordpress-plugin
 * Plugin Name:       Sugar Hills Booking Calendar
 * Plugin URI:        http://ocredg.com
 * Description:       This plugin allows the Sugar Hills Bakery Book your tasting form to submit events to Google Calendar
 * Version:           1.0.0
 * Author:            Nahuel José
 * Author URI:        http://ocredg.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sugar-hills-booking-calendar
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SUGAR_HILLS_BOOKING_CALENDAR_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sugar-hills-booking-calendar-activator.php
 */
function activate_sugar_hills_booking_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sugar-hills-booking-calendar-activator.php';
	Sugar_Hills_Booking_Calendar_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sugar-hills-booking-calendar-deactivator.php
 */
function deactivate_sugar_hills_booking_calendar() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sugar-hills-booking-calendar-deactivator.php';
	Sugar_Hills_Booking_Calendar_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sugar_hills_booking_calendar' );
register_deactivation_hook( __FILE__, 'deactivate_sugar_hills_booking_calendar' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sugar-hills-booking-calendar.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sugar_hills_booking_calendar() {

	$plugin = new Sugar_Hills_Booking_Calendar();
	$plugin->run();

}
run_sugar_hills_booking_calendar();
