<p>You already authorized the plugin to work with your calendar!</p>
<p>If you want to release this authorization, please click the button below.</p>
<p><strong>Keep in mind</strong> that this action will stop the plugin from working and you'll have to authorize it again.</p>