<?php

include_once dirname(dirname(__FILE__)) . '/includes/google-calendar-client/class-sugar-hills-google-calendar.php';

/**
* The admin-specific functionality of the plugin.
*
* @link       http://ocredg.com
* @since      1.0.0
*
* @package    Sugar_Hills_Booking_Calendar
* @subpackage Sugar_Hills_Booking_Calendar/admin
*/

/**
* The admin-specific functionality of the plugin.
*
* Defines the plugin name, version, and two examples hooks for how to
* enqueue the admin-specific stylesheet and JavaScript.
*
* @package    Sugar_Hills_Booking_Calendar
* @subpackage Sugar_Hills_Booking_Calendar/admin
* @author     Nahuel José <nahueljose@gmail.com>
*/
class Sugar_Hills_Booking_Calendar_Admin_Page {
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	* Path to the admin page templates.
	*
	* @var string
	*/
	private $template_path;

	/**
	* URL to google auth
	*
	* @var string
	*/
	private $google_auth_url;
	
	/**
	* Constructor.
	*
	* @param string $template_path
	*/
	public function __construct ( $plugin_name, $version, $template_path)
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->template_path = rtrim($template_path, '/');
	}
	
	/**
	* Configure the admin page using the Settings API.
	*/
	public function configure()
	{
		// Register settings
		register_setting($this->get_slug(), $this->plugin_name . '_settings');

		/**
		 * Here we must check to see if we
		 * already authorized the plugin
		 */
		$google_cal_class = new Sugar_Hills_Google_Calendar( $this->plugin_name, $this->version );
		$google_acess_token = $google_cal_class->getAccessToken();
			
		// Maybe we authorized before. Check if there's an authCode saved
		$options = get_option($this->plugin_name . '_settings');
		$authCode = @trim($options[$this->get_slug() . '-google-auth-code']);

		if ( $google_acess_token === NULL || !$authCode ){
			/**
			 * Unauthorized
			 */
			if( $authCode ){
				// We got ourselves an authcode!
				// Generate the access token
				$google_acess_token = $google_cal_class->fetchAccessTokenWithAuthCode( $authCode );
			} else {
				// NO authcode! Request the user for one
				$this->google_auth_url = $google_cal_class->createAuthUrl();

				// Here we make sure that no access token or auth code is saved
				delete_option($this->plugin_name . '_settings');
				delete_option($this->plugin_name . '_google_access_token');

				// Register section and field
				add_settings_section(
					$this->get_slug() . '-section',
					__('Google Calendar Authorization', $this->plugin_name),
					array($this, 'render_section'),
					$this->get_slug()
				);

				add_settings_field(
					$this->get_slug() . '-google-auth-code',
					__('Authorization Code', $this->plugin_name),
					array($this, 'render_option_field'),
					$this->get_slug(),
					$this->get_slug() . '-section',
					array(
						'id' => $this->get_slug() . '-google-auth-code'
					)
				);
				add_settings_field(
					$this->get_slug() . '-google-calendar-id',
					__('Calendar ID (optional)', $this->plugin_name),
					array($this, 'render_option_field'),
					$this->get_slug(),
					$this->get_slug() . '-section',
					array(
						'id' => $this->get_slug() . '-google-calendar-id'
					)
				);
			}
		} else {
			// We are authorized
			// Register section and field
			add_settings_section(
				$this->get_slug() . '-section_authorized',
				__('Google Calendar Authorization', $this->plugin_name),
				array($this, 'render_section_authorized'),
				$this->get_slug()
			);

			add_settings_field(
				$this->get_slug() . '-google-auth-code',
				__('Release Authorization', $this->plugin_name),
				array($this, 'render_option_field_authorized'),
				$this->get_slug(),
				$this->get_slug() . '-section_authorized',
				array(
					'id' => $this->get_slug() . '-google-auth-code'
				)
			);
			
			add_settings_field(
				$this->get_slug() . '-google-calendar-id',
				__('Delete Calendar ID setting', $this->plugin_name),
				array($this, 'render_option_field_authorized'),
				$this->get_slug(),
				$this->get_slug() . '-section_authorized',
				array(
					'id' => $this->get_slug() . '-google-calendar-id'
				)
			);
		}
	}
	
	/**
	* Get the capability required to view the admin page.
	*
	* @return string
	*/
	public function get_capability()
	{
		return 'manage_options';
	}
	
	/**
	* Get the title of the admin page in the WordPress admin menu.
	*
	* @return string
	*/
	public function get_menu_title()
	{
		return 'SH Book Your Tasting Options';
	}
	
	/**
	* Get the title of the admin page.
	*
	* @return string
	*/
	public function get_page_title()
	{
		return 'Sugar Hills Bakery :: BYT Calendar Options';
	}
	
	/**
	* Get the parent slug of the admin page.
	*
	* @return string
	*/
	public function get_parent_slug()
	{
		return 'options-general.php';
	}
	
	/**
	* Get the slug used by the admin page.
	*
	* @return string
	*/
	public function get_slug()
	{
		return $this->plugin_name;
	}
	
	/**
	* Renders the option field.
	*/
	public function render_option_field($args)
	{
		$this->render_template('option_field', $args);
	}
	public function render_option_field_authorized($args)
	{
		$this->render_template('option_field_authorized', $args);
	}
	
	/**
	* Render the plugin's admin page.
	*/
	public function render_page()
	{
		$this->render_template('page');
	}
	
	/**
	* Render the top section of the plugin's admin page.
	*/
	public function render_section( $args )
	{
		$this->render_template('section');
	}

	public function render_section_authorized( $args )
	{
		$this->render_template('section_authorized');
	}
	
	/**
	* Renders the given template if it's readable.
	*
	* @param string $template
	*/
	private function render_template($template, $args = array())
	{
		$template_path = $this->template_path . '/' . $template . '.php';
		
		if (!is_readable($template_path)) {
			return;
		}
		
		$settings_slug = $this->plugin_name . '_settings';
		$settings = get_option($settings_slug, '');
		$id = isset($args['id']) ? $args['id'] : '';
		include $template_path;
	}
}