<?php

include_once dirname(dirname(__FILE__)) . '/includes/google-calendar-client/class-sugar-hills-google-calendar.php';

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://ocredg.com
 * @since      1.0.0
 *
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Sugar_Hills_Booking_Calendar
 * @subpackage Sugar_Hills_Booking_Calendar/public
 * @author     Nahuel José <nahueljose@gmail.com>
 */
class Sugar_Hills_Booking_Calendar_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sugar_Hills_Booking_Calendar_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sugar_Hills_Booking_Calendar_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if( is_page_template('page-book-tasting.php') ){
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sugar-hills-booking-calendar-public.css', array(), $this->version, 'all' );
		}
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sugar_Hills_Booking_Calendar_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sugar_Hills_Booking_Calendar_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if( is_page_template('page-book-tasting.php') ){
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sugar-hills-booking-calendar-public.js', array( 'jquery' ), $this->version, true );
			wp_localize_script( $this->plugin_name, 'sh_calendar_localization', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );	
		}

	}

	/**
	 * API Actions
	 * Get monthly availability
	 */
	public function get_calendar_availability(){
		$google_cal_class = new Sugar_Hills_Google_Calendar( $this->plugin_name, $this->version );		
		$google_cal_class->init();
		$avail = $google_cal_class->getMonthlyAvailability($_POST['date']);
		wp_send_json_success($avail);
		wp_die();
	}

	public function add_calendar_event($event_details){
		$google_cal_class = new Sugar_Hills_Google_Calendar( $this->plugin_name, $this->version );		
		$google_cal_class->init();
		$event_url = $google_cal_class->add_event($event_details);
		
		return $event_url;
	}

}
