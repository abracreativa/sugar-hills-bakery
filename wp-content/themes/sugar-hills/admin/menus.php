<?php

$site_name = get_bloginfo('name');

/** Registrar el setting. */
function sugar_hills_register_settings() {

	// Registrar settings
	register_setting( 'sugar-hills-options', 'sugar-hills-config' );

	// Secciones
  add_settings_section( 'section-1', __( '&raquo; Contact Info', 'sugar-hills' ), 'sugar_hills_dummy', 'sugar-hills-options' );
  add_settings_section( 'section-2', __( '&raquo; Social Networks', 'sugar-hills' ), 'sugar_hills_dummy', 'sugar-hills-options' );

	// Campos
	add_settings_field( 'e-mail-book-tasting', __( 'Email Book Your Tasting', 'sugar-hills' ), 'field_correo_electronico_1', 'sugar-hills-options', 'section-1' );
	add_settings_field( 'e-mail-customize-cake', __( 'Email Customize your cake', 'sugar-hills' ), 'field_correo_electronico_2', 'sugar-hills-options', 'section-1' );
	add_settings_field( 'e-mail-contact-us', __( 'Email Contact Us', 'sugar-hills' ), 'field_correo_electronico_3', 'sugar-hills-options', 'section-1' );
	// add_settings_field( 'telefono', __( 'Teléfono', 'sugar-hills' ), 'field_telefono', 'sugar-hills-options', 'section-1' );
	// add_settings_field( 'celular', __( 'Celular', 'sugar-hills' ), 'field_celular', 'sugar-hills-options', 'section-1' );
	add_settings_field( 'address-1', __( 'Address 1', 'sugar-hills' ), 'field_direccion_1', 'sugar-hills-options', 'section-1' );
	add_settings_field( 'address-2', __( 'Address 2', 'sugar-hills' ), 'field_direccion_2', 'sugar-hills-options', 'section-1' );

	add_settings_field( 'facebook', __( 'Facebook', 'sugar-hills' ), 'field_facebook', 'sugar-hills-options', 'section-2' );
	add_settings_field( 'instagram', __( 'Instagram', 'sugar-hills' ), 'field_instagram', 'sugar-hills-options', 'section-2' );


}

/** Agregar página de menú. */
function sugar_hills_menu() {
	global $site_name;
	add_menu_page( $site_name . ' Options', $site_name, 'manage_options', 'sugar-hills-options', 'sugar_hills_option','dashicons-admin-generic', 4 );
}

/*
* LA PÁGINA
* Crear la página de configuración
*/
function sugar_hills_option() {
	global $site_name;
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	} ?>
  <div class="wrap">
    <h1><?php echo $site_name; ?></h1>
    <form method="post" action="options.php">

      <?php
      settings_fields( 'sugar-hills-options' );
      do_settings_sections( 'sugar-hills-options' );
      submit_button(); ?>

    </form>
  </div> <!--.wrap-->
<?php } // sugar_hills_option()

/*
* THE SECTIONS
* Hint: You can omit using add_settings_field() and instead
* directly put the input fields into the sections.
* */
function sugar_hills_dummy() {
	// nada
}
/*
* THE FIELDS
* */

function field_facebook() {

	$settings = (array) get_option( 'sugar-hills-config' );
	$field = "facebook";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='http://' name='sugar-hills-config[$field]' value='$value' />";
}
function field_instagram() {

	$settings = (array) get_option( 'sugar-hills-config' );
	$field = "instagram";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='http://' name='sugar-hills-config[$field]' value='$value' />";
}
function field_correo_electronico_1() {

	$settings = (array) get_option( 'sugar-hills-config' );
	$field = "e-mail-book-tasting";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='someone@example.com' name='sugar-hills-config[$field]' value='$value' />";
}
function field_correo_electronico_2() {

	$settings = (array) get_option( 'sugar-hills-config' );
	$field = "e-mail-customize-cake";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='someone@example.com' name='sugar-hills-config[$field]' value='$value' />";
}
function field_correo_electronico_3() {

	$settings = (array) get_option( 'sugar-hills-config' );
	$field = "e-mail-contact-us";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='someone@example.com' name='sugar-hills-config[$field]' value='$value' />";
}
function field_direccion_1() {
	$settings = (array) get_option( 'sugar-hills-config' );

	$field = "address-1-state";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='State' name='sugar-hills-config[$field]' value='$value' /><br>";

	$field = "address-1";
	$value = esc_attr( @$settings[$field] );

	echo "<textarea placeholder='Address' rows='6' cols='20' name='sugar-hills-config[$field]'>$value</textarea><br>";

	$field = "address-1-lat";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='Latitude' name='sugar-hills-config[$field]' value='$value' />";

	$field = "address-1-lng";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='Longitude' name='sugar-hills-config[$field]' value='$value' />";
}
function field_direccion_2() {
	$settings = (array) get_option( 'sugar-hills-config' );

	$field = "address-2-state";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='State' name='sugar-hills-config[$field]' value='$value' /><br>";

	$field = "address-2";
	$value = esc_attr( @$settings[$field] );

	echo "<textarea placeholder='Address' rows='6' cols='20' name='sugar-hills-config[$field]'>$value</textarea><br>";

	$field = "address-2-lat";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='Latitude' name='sugar-hills-config[$field]' value='$value' />";

	$field = "address-2-lng";
	$value = esc_attr( @$settings[$field] );

	echo "<input type='text' placeholder='Longitude' name='sugar-hills-config[$field]' value='$value' />";
}


/** Action: registrar menu */
add_action( 'admin_menu', 'sugar_hills_menu' );
/** Action: registrar setting */
add_action( 'admin_init', 'sugar_hills_register_settings' );
