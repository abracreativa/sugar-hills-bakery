<?php
// Register Custom Post Type Press Item
// Post Type Key: pressitem
function create_pressitem_cpt() {

	$labels = array(
		'name' => __( 'Press', 'Post Type General Name', 'sugar-hills' ),
		'singular_name' => __( 'Press Item', 'Post Type Singular Name', 'sugar-hills' ),
		'menu_name' => __( 'Press', 'sugar-hills' ),
		'name_admin_bar' => __( 'Press Item', 'sugar-hills' ),
		'archives' => __( 'Press Item Archives', 'sugar-hills' ),
		'attributes' => __( 'Press Item Attributes', 'sugar-hills' ),
		'parent_item_colon' => __( 'Parent Press Item:', 'sugar-hills' ),
		'all_items' => __( 'All Press', 'sugar-hills' ),
		'add_new_item' => __( 'Add New Press Item', 'sugar-hills' ),
		'add_new' => __( 'Add New', 'sugar-hills' ),
		'new_item' => __( 'New Press Item', 'sugar-hills' ),
		'edit_item' => __( 'Edit Press Item', 'sugar-hills' ),
		'update_item' => __( 'Update Press Item', 'sugar-hills' ),
		'view_item' => __( 'View Press Item', 'sugar-hills' ),
		'view_items' => __( 'View Press', 'sugar-hills' ),
		'search_items' => __( 'Search Press Item', 'sugar-hills' ),
		'not_found' => __( 'Not found', 'sugar-hills' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'sugar-hills' ),
		'featured_image' => __( 'Featured Image', 'sugar-hills' ),
		'set_featured_image' => __( 'Set featured image', 'sugar-hills' ),
		'remove_featured_image' => __( 'Remove featured image', 'sugar-hills' ),
		'use_featured_image' => __( 'Use as featured image', 'sugar-hills' ),
		'insert_into_item' => __( 'Insert into Press Item', 'sugar-hills' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Press Item', 'sugar-hills' ),
		'items_list' => __( 'Press list', 'sugar-hills' ),
		'items_list_navigation' => __( 'Press list navigation', 'sugar-hills' ),
		'filter_items_list' => __( 'Filter Press list', 'sugar-hills' ),
	);

	$args = array(
		'label' => __( 'Press Item', 'sugar-hills' ),
		'description' => __( '', 'sugar-hills' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-aside',
		'supports' => array('title', 'excerpt', 'thumbnail', 'revisions', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);

	register_post_type( 'press', $args );

}
add_action( 'init', 'create_pressitem_cpt', 0 );
