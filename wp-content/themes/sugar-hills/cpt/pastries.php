<?php
// Register Custom Post Type Pastry
// Post Type Key: pastry
function create_pastry_cpt() {

	$labels = array(
		'name' => __( 'Pastries', 'Post Type General Name', 'sugar-hills' ),
		'singular_name' => __( 'Pastry', 'Post Type Singular Name', 'sugar-hills' ),
		'menu_name' => __( 'Pastries', 'sugar-hills' ),
		'name_admin_bar' => __( 'Pastry', 'sugar-hills' ),
		'archives' => __( 'Pastry Archives', 'sugar-hills' ),
		'attributes' => __( 'Pastry Attributes', 'sugar-hills' ),
		'parent_item_colon' => __( 'Parent Pastry:', 'sugar-hills' ),
		'all_items' => __( 'All Pastries', 'sugar-hills' ),
		'add_new_item' => __( 'Add New Pastry', 'sugar-hills' ),
		'add_new' => __( 'Add New', 'sugar-hills' ),
		'new_item' => __( 'New Pastry', 'sugar-hills' ),
		'edit_item' => __( 'Edit Pastry', 'sugar-hills' ),
		'update_item' => __( 'Update Pastry', 'sugar-hills' ),
		'view_item' => __( 'View Pastry', 'sugar-hills' ),
		'view_items' => __( 'View Pastries', 'sugar-hills' ),
		'search_items' => __( 'Search Pastry', 'sugar-hills' ),
		'not_found' => __( 'Not found', 'sugar-hills' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'sugar-hills' ),
		'featured_image' => __( 'Featured Image', 'sugar-hills' ),
		'set_featured_image' => __( 'Set featured image', 'sugar-hills' ),
		'remove_featured_image' => __( 'Remove featured image', 'sugar-hills' ),
		'use_featured_image' => __( 'Use as featured image', 'sugar-hills' ),
		'insert_into_item' => __( 'Insert into Pastry', 'sugar-hills' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Pastry', 'sugar-hills' ),
		'items_list' => __( 'Pastries list', 'sugar-hills' ),
		'items_list_navigation' => __( 'Pastries list navigation', 'sugar-hills' ),
		'filter_items_list' => __( 'Filter Pastries list', 'sugar-hills' ),
	);
	$args = array(
		'label' => __( 'Pastry', 'sugar-hills' ),
		'description' => __( '', 'sugar-hills' ),
		'labels' => $labels,
		'menu_icon' => get_template_directory_uri() . '/svg/croissant.svg',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', ),
		'taxonomies' => array('pastry_category', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'pastries', $args );

}
add_action( 'init', 'create_pastry_cpt', 0 );

// Register Taxonomy Pastry Category
// Taxonomy Key: pastrycategory
function create_pastrycategory_tax() {

	$labels = array(
		'name'              => _x( 'Pastry Categories', 'taxonomy general name', 'sugar-hills' ),
		'singular_name'     => _x( 'Pastry Category', 'taxonomy singular name', 'sugar-hills' ),
		'search_items'      => __( 'Search Pastry Categories', 'sugar-hills' ),
		'all_items'         => __( 'All Pastry Categories', 'sugar-hills' ),
		'parent_item'       => __( 'Parent Pastry Category', 'sugar-hills' ),
		'parent_item_colon' => __( 'Parent Pastry Category:', 'sugar-hills' ),
		'edit_item'         => __( 'Edit Pastry Category', 'sugar-hills' ),
		'update_item'       => __( 'Update Pastry Category', 'sugar-hills' ),
		'add_new_item'      => __( 'Add New Pastry Category', 'sugar-hills' ),
		'new_item_name'     => __( 'New Pastry Category Name', 'sugar-hills' ),
		'menu_name'         => __( 'Pastry Categories', 'sugar-hills' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'sugar-hills' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'pastry_category', array('pastries', ), $args );

}
add_action( 'init', 'create_pastrycategory_tax' );
