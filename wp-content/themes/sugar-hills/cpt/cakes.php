<?php
// Register Custom Post Type Cake
// Post Type Key: cake
function create_cake_cpt() {

	$labels = array(
		'name' => __( 'Cakes', 'Post Type General Name', 'sugar-hills' ),
		'singular_name' => __( 'Cake', 'Post Type Singular Name', 'sugar-hills' ),
		'menu_name' => __( 'Cakes', 'sugar-hills' ),
		'name_admin_bar' => __( 'Cake', 'sugar-hills' ),
		'archives' => __( 'Cake Archives', 'sugar-hills' ),
		'attributes' => __( 'Cake Attributes', 'sugar-hills' ),
		'parent_item_colon' => __( 'Parent Cake:', 'sugar-hills' ),
		'all_items' => __( 'All Cakes', 'sugar-hills' ),
		'add_new_item' => __( 'Add New Cake', 'sugar-hills' ),
		'add_new' => __( 'Add New', 'sugar-hills' ),
		'new_item' => __( 'New Cake', 'sugar-hills' ),
		'edit_item' => __( 'Edit Cake', 'sugar-hills' ),
		'update_item' => __( 'Update Cake', 'sugar-hills' ),
		'view_item' => __( 'View Cake', 'sugar-hills' ),
		'view_items' => __( 'View Cakes', 'sugar-hills' ),
		'search_items' => __( 'Search Cake', 'sugar-hills' ),
		'not_found' => __( 'Not found', 'sugar-hills' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'sugar-hills' ),
		'featured_image' => __( 'Featured Image', 'sugar-hills' ),
		'set_featured_image' => __( 'Set featured image', 'sugar-hills' ),
		'remove_featured_image' => __( 'Remove featured image', 'sugar-hills' ),
		'use_featured_image' => __( 'Use as featured image', 'sugar-hills' ),
		'insert_into_item' => __( 'Insert into Cake', 'sugar-hills' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Cake', 'sugar-hills' ),
		'items_list' => __( 'Cakes list', 'sugar-hills' ),
		'items_list_navigation' => __( 'Cakes list navigation', 'sugar-hills' ),
		'filter_items_list' => __( 'Filter Cakes list', 'sugar-hills' ),
	);
	$args = array(
		'label' => __( 'Cake', 'sugar-hills' ),
		'description' => __( '', 'sugar-hills' ),
		'labels' => $labels,
		'menu_icon' => get_template_directory_uri() . '/svg/cake.svg',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', ),
		'taxonomies' => array('cake_category', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'cakes', $args );

}
add_action( 'init', 'create_cake_cpt', 0 );

// Register Taxonomy Cake Category
// Taxonomy Key: cakecategory
function create_cakecategory_tax() {

	$labels = array(
		'name'              => _x( 'Cake Categories', 'taxonomy general name', 'sugar-hills' ),
		'singular_name'     => _x( 'Cake Category', 'taxonomy singular name', 'sugar-hills' ),
		'search_items'      => __( 'Search Cake Categories', 'sugar-hills' ),
		'all_items'         => __( 'All Cake Categories', 'sugar-hills' ),
		'parent_item'       => __( 'Parent Cake Category', 'sugar-hills' ),
		'parent_item_colon' => __( 'Parent Cake Category:', 'sugar-hills' ),
		'edit_item'         => __( 'Edit Cake Category', 'sugar-hills' ),
		'update_item'       => __( 'Update Cake Category', 'sugar-hills' ),
		'add_new_item'      => __( 'Add New Cake Category', 'sugar-hills' ),
		'new_item_name'     => __( 'New Cake Category Name', 'sugar-hills' ),
		'menu_name'         => __( 'Cake Categories', 'sugar-hills' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( 'Categories for Cakes', 'sugar-hills' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => false,
		'show_tagcloud' => true,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'cake_category', array('cakes', ), $args );

}
add_action( 'init', 'create_cakecategory_tax' );
