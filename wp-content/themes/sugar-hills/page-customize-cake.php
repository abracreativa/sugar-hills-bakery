<?php
/**
 * Template Name: Customize your cake
 *
 * @package Sugar_Hills_Bakery
 */

 include TEMPLATEPATH . '/_cake_variables.php';
 get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
				?>

				<form class="customize-your-cake-form" action="<?php echo get_template_directory_uri(); ?>/sendmail/sendmail.php" method="post" enctype="multipart/form-data">

          <?php if( isset($_GET['mailerror']) ):
            echo sprintf( '<p class="sugar-hills-message sh-msg-%s">%s</p>',
            $_GET['mailerror'] == '1' ? 'error' : 'success',
            urldecode($_GET['message'])
          );
           endif; ?>

          <h2>Flavor Options<br> <label for="Flavor_Options[]" class="error"></label></h2>

          <h3><label><input class="required" type="radio" name="Flavor_Options[]" id="Flavor_Options" value="Signature Flavors" data-toggle-item="#flavor_options_signature"> Signature Flavors</label></h3>

          <div id="flavor_options_signature" class="radio-toggle-container disabled">
            <p>We’ve created the perfect flavor combinations for you. Would you like to pick from our signature flavors below?</p>
  					<p><strong>Mark your flavor of choice</strong><br>
              <label for="Flavor_Options[Signature_Flavor]" class="error"></label></p>
  					<p class="sugar-hills-text-columns">
  						<?php foreach ($everyday_cakes as $_flavor_option):
  							echo sprintf( '<label><input class="required" type="radio" name="Flavor_Options[Signature_Flavor]" id="Flavor_Options[Signature_Flavor]" value="%1$s"> %1$s</label><br>', $_flavor_option );
  						endforeach; ?>
  					</p>
  					<div class="sugar-hills-highlight">
  					  <p>If you would like to view our everyday cake gallery <a target="_blank" href="<?php echo sugar_hills_get_permalink( 'everyday-cakes', 'term', 'cake_category' ); ?>" target="_blank">click here</a>.</p>
  					</div>
          </div>

          <h3><label><input class="required" type="radio" name="Flavor_Options[]" value="Build your own" data-toggle-item="#flavor_options_build"> Build your own</label></h3>

          <div id="flavor_options_build" class="radio-toggle-container disabled">
            <p>If you would like to build your own flavor combination select from the following:</p>

            <h4 class="separator-title">Cake Options<br><label for="Flavor_Options[Cake_option]" class="error"></label></h4>
            <p class="sugar-hills-text-columns">
              <?php foreach ($cake_options as $_cake_option):
                echo sprintf( '<label><input class="required" type="radio" name="Flavor_Options[Cake_option]" id="Flavor_Options[Cake_option]" value="%1$s"> %1$s</label><br>', $_cake_option );
              endforeach; ?>
            </p>

            <h4 class="separator-title">Filling Options <label for="Flavor_Options[Filling_options][]" class="error"></label></h4>

            <?php foreach ($filling_options as $key => $_filling_option):
              echo sprintf( '<h5><label><input class="required" type="radio" name="Flavor_Options[Filling_options][]" id="Flavor_Options[Filling_options][]" data-toggle-item="%3$s" value="%1$s"> %1$s <small>%2$s</small></label></h5>', $_filling_option['title'], $_filling_option['description'], '#filling_options_' . $key );
              echo '<div class="radio-toggle-container disabled" id="filling_options_'.$key.'">';
              echo '<p class="sugar-hills-text-columns"><label for="Flavor_Options[Filling_options]" class="error"></label>';
              foreach ($_filling_option['options'] as $_fop) {
                echo sprintf( '<label><input  class="required" type="radio" name="Flavor_Options[Filling_options]" id="Flavor_Options[Filling_options]" value="%2$s: %1$s"> %1$s</label><br>', $_fop, $_filling_option['title'] );
              }
              echo '</p></div>';
            endforeach; ?>
          </div>

          <h2>Cake Size<br><label for="Cake_Size" class="error"></label></h2>
          <p class="sugar-hills-text-columns">
            <?php foreach ( $cake_sizes as $_cake_size ):
              echo sprintf( '<label><input class="required" type="radio" name="Cake_Size" id="Cake_Size" value="%1$s"> %1$s</label><br>', $_cake_size );
            endforeach; ?>
          </p>
          <p><strong>For tiered cakes</strong></p>
          <p class="sugar-hills-text-columns">
            <?php foreach ( $cake_sizes_2 as $_cake_size ):
              echo sprintf( '<label><input class="required" type="radio" name="Cake_Size" id="Cake_Size" value="%1$s"> %1$s</label><br>', $_cake_size );
            endforeach; ?>
          </p>
          <div class="sugar-hills-highlight">
            <p>Custom shapes &amp; sizes available.  Are Individually priced.</p>
          </div>

          <h2>Decoration Options</h2>
          <p>We’ve have also created a variety of designs from a simple everyday cake to any themed special occasion cake.</p>
          <p><strong>Pick from our everyday cakes, special occasion cakes or submit your own design.</strong></p>
          <div class="sugar-hills-highlight">
            <p>Click here to view our galleries for <a target="_blank" href="<?php echo sugar_hills_get_permalink( 'everyday-cakes', 'term', 'cake_category' ); ?>">Everyday Cake​ designs</a> or our ​<a target="_blank" href="<?php echo sugar_hills_get_permalink( 'special-occasion-cakes', 'term', 'cake_category' ); ?>">Special Occasion​ designs</a>.</p>
          </div>

          <p><label for="Decoration_Options[]" class="error"></label></p>

          <h4><label><input type="radio" class="required" name="Decoration_Options[]" id="Decoration_Options[]" value="Everyday Cakes" data-toggle-item="#decoration_edc"> Everyday Cakes</label></h4>

          <div class="radio-toggle-container disabled" id="decoration_edc">
            <p class="sugar-hills-text-columns">
              <label for="Decoration_Options[Every_day_cake]" class="error"></label>
              <?php foreach ($everyday_cakes as $_flavor_option):
                echo sprintf( '<label><input class="required" type="radio" name="Decoration_Options[Every_day_cake]" id="Decoration_Options[Every_day_cake]" value="%1$s"> %1$s</label><br>', $_flavor_option );
              endforeach; ?>
            </p>
          </div>

          <h4><label><input class="required" type="radio" name="Decoration_Options[]" id="Decoration_Options[]" value="Special Occasion Cakes" data-toggle-item="#decoration_soc"> Special Occasion Cakes</label></h4>
          <div class="radio-toggle-container disabled" id="decoration_soc">
            <p>Please provide the number of the cake you're interested in.</p>
            <p><label>Cake number:</label> <input class="required" type="number" name="Decoration_Options[Special_Occasion_Cake]" id="Decoration_Options[Special_Occasion_Cake]" placeholder="#0000"></p>
            <div class="sugar-hills-highlight">
              <p> You can browse our ​<a target="_blank" href="<?php echo sugar_hills_get_permalink( 'special-occasion-cakes', 'term', 'cake_category' ); ?>">Special Occasion​ designs</a> to find the number.</p>
            </div>
          </div>

          <h4><label><input class="required" type="radio" name="Decoration_Options[]" id="Decoration_Options[]" value="Submit Your Own Design [see file attached]" data-toggle-item="#decoration_own"> Submit Your Own Design</label></h4>
          <div class="radio-toggle-container disabled" id="decoration_own">
            <p>Please attach an illustration of it here (Upload GIF, JPG, PDF formats
            only): <input class="required" type="file" name="file" id="file"></p>
          </div>

          <p>Do you need a customized inscription? <input type="text" name="Decoration_Options[Customized_inscription]" id="Decoration_Options[Customized_inscription]"></p>

          <h2>Delivery &amp; Pick Up Information<br><label for="Delivery_and_pick_up_information[]" class="error"></label></h2>
          <p>
						<?php foreach ($delivery_options as $_delivery_option):
							echo sprintf( '<label><input class="required" type="radio" name="Delivery_and_pick_up_information[]" id="Delivery_and_pick_up_information[]" value="%1$s"> %1$s</label><br>', $_delivery_option );
						endforeach; ?>
					</p>
          <p>Pick a delivery or pick-up date: <input class="required" type="date" name="Delivery_and_pick_up_information[Delivery_or_pick_up_date]" id="Delivery_and_pick_up_information[Delivery_or_pick_up_date]"><br><small>(Delivery is available Monday - Saturday only)</small></p>

          <h2>Contact Information</h2>
          <div class="form-text-fields">
            <p><label>Full Name</label> <input class="required" type="text" name="Contact_information[Full_Name]" id="Contact_information[Full_Name]"></p>
            <p><label>Your Address</label> <input class="required" type="text" name="Contact_information[Your_Address]" id="Contact_information[Your_Address]"></p>
            <p><label>Your City</label> <input class="required" type="text" name="Contact_information[Your_City]" id="Contact_information[Your_City]"></p>
            <p><label>Your State</label> <input class="required" type="text" name="Contact_information[Your_State]" id="Contact_information[Your_State]"></p>
            <p><label>Your Zip Code</label> <input class="required" type="text" name="Contact_information[Your_Zip_Code]" id="Contact_information[Your_Zip_Code]"></p>
            <p><label>Email</label> <input class="required" type="email" name="Contact_information[Email]" id="Contact_information[Email]"></p>
            <p><label>Telephone</label> <input class="required" type="tel" name="Contact_information[Telephone]" id="Contact_information[Telephone]"></p>
            <p><label>Alternate Telephone</label> <input type="tel" name="Contact_information[Alternate_Telephone]" id="Contact_information[Alternate_Telephone]"></p>
            <h3>Delivery Address (if different)</h3>
            <p><label>Address</label> <input type="text" name="Contact_information[Delivery_address][Address]" id="Contact_information[Delivery_address][Address]"></p>
            <p><label>City</label> <input type="text" name="Contact_information[Delivery_address][City]" id="Contact_information[Delivery_address][City]"></p>
            <p><label>State</label> <input type="text" name="Contact_information[Delivery_address][State]" id="Contact_information[Delivery_address][State]"></p>
            <p><label>Zip Code</label> <input type="text" name="Contact_information[Delivery_address][Zip Code]" id="Contact_information[Delivery_address][Zip Code]"></p>
            <p>
              <div class="g-recaptcha" data-sitekey="6Lc-VycUAAAAABpVDdZXCNSBnxp2VtdSP42c69Us"></div>
            </p>
          </div>

          <p>
            <input type="hidden" name="Form_submit" id="Form_submit" value="1">
            <input type="hidden" name="Form" id="Form" value="e-mail-customize-cake">
            <input type="hidden" name="Return_To" id="Return_To" value="<?php the_permalink(); ?>">
            <input type="hidden" name="Contact_information[Subject]" id="Contact_information[Subject]" value="Customize your cake">
  					<button type="submit" class="sugar-hills-button button-l"><span>Request Quote</span></button>
          </p>
				</form>

			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
