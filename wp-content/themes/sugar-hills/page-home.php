<?php
/**
 * Template Name: Home
 *
 * @package Sugar_Hills_Bakery
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="home-slider-wrap">
				<section class="home-slider owl-carousel">
					<?php
						/**
						* Slider
						*/
						$slides = get_field('home-slider');
						if( $slides ): foreach ($slides as $slide):

							echo sprintf( '<article class="home-slider-slide" style="background-image:url(%s)">', $slide['slide-image']['sizes']['wide-header'] );
							echo sprintf( '<img src="%s" alt="%s">', $slide['slide-image']['sizes']['wide-header'], esc_attr( $slide['slide-title'] ) );
							echo '<div class="home-slider-text">';
							$sub_title = '';
							if( $slide['slide-sub-title'] ){
								$sub_title = ' <small>' . $slide['slide-sub-title'] . '</small>';
							}
							echo sprintf( '<h1>%s%s</h1>', $slide['slide-title'], $sub_title );
							if( $slide['slide-button-text'] && $slide['slide-button-link'] ){
								echo sprintf( '<a href="%s" class="home-slider-button">%s</a>', $slide['slide-button-link'], $slide['slide-button-text'] );
							}
							echo '</div>';
							echo '</article>';

						endforeach; endif;
					?>

				</section>
				<div class="content-wrap home-slider-buttons">
					<?php
						echo sprintf( '<button class="home-slider-prev">%s</button>', sugar_hills_get_svg('arrow-prev') );
						echo sprintf( '<button class="home-slider-next">%s</button>', sugar_hills_get_svg('arrow-next') );
					?>
				</div>
			</div><!--.home-slider-wrap -->

			<section class="home-main-banner">
				<div class="content-wrap">
					<?php
						/**
						* Main Banner
						*/
						$_img = get_field('home-banner-image');
						?>
						<a href="<?php the_field('home-banner-link'); ?>">
							<div class="home-banner-text">
								<h2 class="home-banner-title"><?php the_field('home-banner-title'); ?></h2>
								<div class="home-banner-hover-text">
									<?php the_field('home-banner-hover-text'); ?>
								</div>
							</div>
							<img src="<?php echo $_img['sizes']['large']; ?>" alt="<?php echo esc_attr( get_field('home-banner-title') ); ?>">
						</a>
				</div>
			</section>

			<section class="home-banners-row">
				<?php
					/**
					* Banners Row
					*/
					$banners = get_field( 'home-bottom-banners' );

					foreach ($banners as $_banner) {
						$_target = $_banner['home-bottom-banner-link-newtab'] ? 'target="_blank"' : '';
						echo sprintf( '<a %s class="home-bottom-banner" href="%s" style="background-image:url(%s)">', $_target, $_banner['home-bottom-banner-link'], $_banner['home-bottom-banner-image']['sizes']['large'] );
						?>
						<div class="flip-container">
							<div class="flipper">
								<div class="flip-front">
									<?php echo sugar_hills_clean_svg($_banner['home-bottom-banner-logo']['url']); ?>
								</div>
								<div class="flip-back">
									<?php echo $_banner['home-bottom-banner-text']; ?>
								</div>
							</div>
						</div>
						<?php echo '</a>';
					}
					?>
			</section>

			<?php endwhile; endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
