<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sugar_Hills_Bakery
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/icons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/icons/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/icons/manifest.json">
<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/icons/safari-pinned-tab.svg" color="#94c1ad">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/icons/favicon.ico">
<meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/icons/browserconfig.xml">
<meta name="theme-color" content="#94c1ad">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php
	$not_home = !(is_home() || is_front_page()) ? 'not-home' : '';
	body_class( $not_home );
?>>
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner" <?php sugar_hills_header_image(); ?>>
		<div class="header-inner-wrap">
			<div class="content-wrap">
				<div class="site-branding">
					<a href="<?php echo home_url('/'); ?>">
						<?php echo sugar_hills_get_svg('logo'); ?>
					</a>
				</div><!-- .site-branding -->

				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'sugar-hills' ); ?></button>
					<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div><!-- .content-wrap -->
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
