<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sugar_Hills_Bakery
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php
				if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
							sugar_hills_breadcrumbs();
							sugar_hills_the_archive_title( '<h1 class="page-title">', '</h1>', true );
							sugar_hills_archive_nav();
						?>
					</header><!-- .page-header -->

					<section class="sugar-hills-grid bricklayer" id="ajax-content">
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('sugar-hills-grid-item'); ?>>
							<?php
								if( get_post_type() == 'pastries' ){
									echo '<a>';
								} else {
									echo '<a href="'.esc_url( get_permalink()).'">';
								}
									the_post_thumbnail( 'medium' );
									the_title( '<h2 class="entry-title">', '</h2>' );
								?>
							</a>
						</article><!-- #post-## -->

						<?php endwhile; ?>

					</section><!-- .sugar-hills-grid -->

				<?php

				the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
