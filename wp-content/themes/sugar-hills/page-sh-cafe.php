<?php
/**
 * Template Name: Sugar Hills Café
 *
 * @package Sugar_Hills_Bakery
 */


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="page-header">
							<?php sugar_hills_breadcrumbs(); ?>
						</header><!-- .entry-header -->

						<div class="entry-content sugar-hills-cafe-wrap">
							<div class="sugar-hills-cafe-menu">
								<?php wp_nav_menu( array(
									'container' => false,
									'theme_location' => 'menu-sh-cafe',
									'menu_id' => 'menu-sh-cafe'
								) ); ?>
							</div>
								<div class="sugar-hills-cafe-text">
								<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
									<?php
										if( get_field( 'post-first-paragraph' ) ){
											echo sprintf( '<div class="sugar-hills-first-paragraph">%s</div>', wpautop(get_field('post-first-paragraph')) );
										}

										$_image = get_field( 'sh-cafe-image' );
										if( $_image ){
											echo '<div class="sugar-hills-cafe-text-content">';
											echo sprintf( '<figure class="sugar-hills-cafe-text-image"><img src="%s" alt="%s"></figure>', $_image['sizes']['large'], get_the_title() );
											echo '<div>';
											the_content();
											echo '</div>';
											echo '</div>';
										} else {
											the_content();
										}
										?>
								</div>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->

				<?php endwhile; // End of the loop. ?>
			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
