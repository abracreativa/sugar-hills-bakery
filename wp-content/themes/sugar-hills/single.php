<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Sugar_Hills_Bakery
 */

global $post;

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header <?php echo is_single() ? 'page-header' : ''; ?>">
							<?php
								sugar_hills_breadcrumbs();
								the_title( '<h1 class="entry-title">', '</h1>' );
							?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<div class="row">
								<div class="col col-sm-6 the-content-wrap">
									<?php
										if( get_field( 'post-first-paragraph' ) ){
											echo sprintf( '<div class="sugar-hills-first-paragraph">%s</div>', wpautop(get_field('post-first-paragraph')) );
										}
										the_content();
										$categories = wp_get_object_terms( $post->ID, 'cake_category' );
										foreach ($categories as $_cat) {
											if( get_field( 'cake-category-customizeable', $_cat ) ){
												// Option is set to true ?>
												<p>
													<a href="<?php echo get_permalink( get_page_by_path('book-your-tasting') ); ?>?cake_id=<?php the_ID(); ?>" class="sugar-hills-button-simple">Book your tasting</a>
												</p>
											<?php
											}
											echo '<p>';
											if( get_field( 'cake-category-flavor-options-button', $_cat ) ){	?>
												<a href="<?php echo get_permalink( get_page_by_path('flavor-options') ); ?>" class="sugar-hills-button"><span>Flavor Options</span></a>
												<?php
											}
											if( get_field( 'cake-category-customize-button', $_cat ) ){ ?>
												<a href="<?php echo get_permalink( get_page_by_path('customize-your-cake') ); ?>?cake_id=<?php the_ID(); ?>" class="sugar-hills-button"><span>Customize Your Cake</span></a>
												<?php
											}
											echo '</p>';
											break;
										}
									?>

								</div>
								<div class="col col-sm-6">
									<?php sugar_hills_the_post_thumbnail('large', false); ?>
								</div>
							</div>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->

				<?php endwhile; // End of the loop. ?>

			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
