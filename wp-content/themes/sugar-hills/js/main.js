/**
 * Archivo principal de JavaScript
 *
 * @package Sugar_Hills_Bakery
 */
;(function($){

  window.SUGAR = {

    /**
     * Init
     */
    init: function(){
      // Add class
      $('body').addClass('js');
      // Init methods
      this.home_slider();
      this.fixed_header();
      this.masonry();
      this.infinite_scrolling();
      this.gmaps();
      this.form_utils();
      this.back_to_top();
    },

    /**
     * Home Slider
     */
    home_slider: function(){
      var owl = $('.home-slider');
      owl.owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplaySpeed: 1000,
        nav: false,
      });
      // Go to the next item
      $('.home-slider-next').click(function() {
        owl.trigger('next.owl.carousel');
      })
      // Go to the previous item
      $('.home-slider-prev').click(function() {
        owl.trigger('prev.owl.carousel');
      })
    },

    /**
     * Fixed Header
     */
    fixed_header: function(){
      var $window = $(window);
      var $header = $('.site-header');
      var $body = $('body');
      var scroll = 0;
      $window.on('scroll', function(){
        scroll = $window.scrollTop();
        $header.toggleClass('fixed-header', scroll >= 50);
        $body.toggleClass('scrolling', scroll >= 250);
      });
      $window.trigger('scroll');
    },

    /**
     * Back to top
     */
    back_to_top: function(){
      $body = $('html, body');
      $('.back-to-top').on('click', function(){
         $body.animate({
            scrollTop: 0
          }, 800);
      });
    },

    /**
     * Masonry Layout
     */
    masonry: function(){
      var _self = this;
      var grid_container = document.querySelector('.bricklayer');

      if( grid_container ){
        $('.bricklayer').imagesLoaded( function(){
          _self.bricklayer = new Bricklayer( grid_container );
          // Initially loaded items
          $('.sugar-hills-grid-item').addClass('loaded');

          // Add class after append
          _self.bricklayer.on("afterAppend", function (e) {
            var el = e.detail.item;
            var $el = $(el);
            // Add loaded class once images have loaded
            $el.imagesLoaded(function(){
              $el.addClass('loaded');
            });
          });
        });
      }

    },

    /**
     * Infinite Scrolling
     */
    infinite_scrolling: function(){
      var _self       = this;
      var $window     = $(window);
      var $nav        = $('.posts-navigation');
      var $items_grid = $('#ajax-content');
      var prev_url    = '';
      var next_url    = '';

      var update_items = function( responseText ){
        $response = $( "<div>" ).append( $.parseHTML( responseText ) );
        // Find items
        $items    = $response.find( '#ajax-content' );
        $new_nav  = $response.find( '.posts-navigation' );
        // Append new items
        // Get items
        $items = $items.find('.sugar-hills-grid-item');
        $items.each( function(){
          var $this = $(this);
          // // Le resto 10 por el padding de 5px que tienen los items
          var ancho_base = $('.bricklayer-column-sizer').width();
          // // Verificamos el ancho y alto de la imagen
          // // esto es necesario para que el plugin calcule correctamente
          // // la posición del nuevo item
          img = $this.find('img');
          $this.css({
            'width' : ancho_base,
            'height' : ancho_base * ( parseInt( img.attr('height') ) / parseInt(img.attr('width')) ),
          });
          _self.bricklayer.append( $this[0] );
        });
        // Replace navigation with new links
        $nav.html( $new_nav.html() );
      }

      var check_viewport = _self.debounce(function(){
        if( _self.isScrolledIntoView( $window, $nav ) ){
          // The bottom was reached, call for more items
          next_url = $nav.find('.nav-previous a').attr('href');
          if( next_url && (prev_url != next_url) ){
            // Cache prev_url
            prev_url = next_url;
            // Get more items
            $.ajax({
              url: next_url,
            }).done( update_items );
          } else {
            // No more items to get
            $nav.addClass('no-more-items');
          }
        }
      }, 250);

      if( $nav.length ){
        $window.on('scroll', check_viewport);
        $window.trigger('scroll');
      }
    },

    gmaps: function(){

      $maps = $('.g-map');

      if( $maps.length ){
        $maps.each( function(){
          var $this = $(this);

          var locations = [
            [ $this.attr('data-lat'), $this.attr('data-lng')]
          ];
          var map = new google.maps.Map( $this[0] , {
            zoom: 15,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false,
            scrollwheel: false,
            center: new google.maps.LatLng( $this.attr('data-lat'), $this.attr('data-lng') ),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });

          var marker, i;

          for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][0], locations[i][1]),
              map: map
            });
          }

        });
      }

    },

    /**
    * UTILS
    */
    isScrolledIntoView: function ($window, $elem){
      var docViewTop = $window.scrollTop();
      var docViewBottom = docViewTop + $window.height();

      var elemTop = $elem.offset().top;
      var elemBottom = elemTop + $elem.height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    },

    debounce: function(func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) func.apply(context, args);
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow) func.apply(context, args);
    	};
    },

    form_utils: function(func, wait, immediate) {
    	// Toggle element depending on radio button status
      var $toggle_radios = $('[data-toggle-item]');
      $toggle_radios.on('change', function(){
        // We hide the containers of all the other radios
        var $this = $(this);
        var $other_radios = $( 'input[name="'+ $this.attr('name') +'"]' ).not( $this );
        $other_radios.each( function(){
          $( $(this).attr('data-toggle-item') )
            .slideUp()
            .addClass('disabled')
            .find('input, textarea')
            .prop('checked', false)
            .attr('disabled', true);
        });

        // Show the container
        if( $this.is(':checked') ){
          $( $this.attr('data-toggle-item') )
          .slideDown()
          .removeClass('disabled')
          .find('input, textarea')
          .attr('disabled', false);
        }
      }).trigger('change');

      // Datepicker
      $('input[type="date"]')
        .attr('type', 'text')
        .datepicker({
          beforeShowDay: function(date) {
            var day = date.getDay();
            return [(day != 0), ''];
          },
          onSelect: function(dateText, inst) {
            // Tasting date & time
            var $date_input = $('#Tasting_Date');
            var $time_input = $('#Tasting_Time');
            var date = $date_input.datepicker( "getDate" );
            $time_input.val('');
            // Get weekday
            if( date.getDay() == 6 ){
              // Saturdays
              $time_input.find('.datetime-weekdays').hide();
              $time_input.find('.datetime-weekend').show();
            } else {
              // Rest of the week
              $time_input.find('.datetime-weekdays').show();
              $time_input.find('.datetime-weekend').hide();
            }
          }
        });

      // Validator
      $('form').each(function(){
        $(this).validate({
          submitHandler: function(form){
            $(form).find('button[type="submit"]')
              .attr('disabled', true)
              .find('span').text('Please wait...');

            form.submit();
          }
        });
      });

      // Checkbox Limit
      $('[data-max-checked]').on('change', function(evt) {
        limit = parseInt( $(this).attr('data-max-checked') );
        siblings = $( '[name="'+ $(this).attr('name') +'"]:checked' );

        if( limit && siblings.length > limit ) {
          this.checked = false;
        }
      });
    },

  };

})(jQuery);

/* Init */
SUGAR.init();
