<?php

define('ACF_LITE', true);

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_other-options',
		'title' => 'Other options',
		'fields' => array (
			array (
				'key' => 'field_5925bc4078b67',
				'label' => 'Featured Image',
				'name' => 'category-featured-image',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'all',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_home',
		'title' => 'Home',
		'fields' => array (
			array (
				'key' => 'field_592c2d0e9290a',
				'label' => 'Slider',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_592834e986fff',
				'label' => 'Slider',
				'name' => 'home-slider',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_5928374887000',
						'label' => 'Title',
						'name' => 'slide-title',
						'type' => 'text',
						'required' => 1,
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5928388c87001',
						'label' => 'Sub-title',
						'name' => 'slide-sub-title',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => 40,
					),
					array (
						'key' => 'field_5928399b87002',
						'label' => 'Button text',
						'name' => 'slide-button-text',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => 40,
					),
					array (
						'key' => 'field_592839c787003',
						'label' => 'Button Link',
						'name' => 'slide-button-link',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_59284c83c3d11',
						'label' => 'Image',
						'name' => 'slide-image',
						'type' => 'image',
						'required' => 1,
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => 2,
				'row_limit' => 6,
				'layout' => 'row',
				'button_label' => 'Add Slide',
			),
			array (
				'key' => 'field_592c2b845874c',
				'label' => 'Main banner',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_592c2bed5874d',
				'label' => 'Title',
				'name' => 'home-banner-title',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_592c2c0c5874e',
				'label' => 'Image',
				'name' => 'home-banner-image',
				'type' => 'image',
				'required' => 1,
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_592c2c265874f',
				'label' => 'Hover Text',
				'name' => 'home-banner-hover-text',
				'type' => 'wysiwyg',
				'instructions' => 'Should be very short. Last line in bold will show larger.',
				'required' => 1,
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
			array (
				'key' => 'field_592c2dc70a546',
				'label' => 'Link',
				'name' => 'home-banner-link',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_592c44caca342',
				'label' => 'Banners Row',
				'name' => '',
				'type' => 'tab',
			),
			array (
				'key' => 'field_592c44deca343',
				'label' => 'Banners',
				'name' => 'home-bottom-banners',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_592c44ffca344',
						'label' => 'Background Image',
						'name' => 'home-bottom-banner-image',
						'type' => 'image',
						'required' => 1,
						'column_width' => '',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_592c45efca345',
						'label' => 'Logo',
						'name' => 'home-bottom-banner-logo',
						'type' => 'file',
						'instructions' => 'SVG File',
						'required' => 1,
						'column_width' => '',
						'save_format' => 'object',
						'library' => 'all',
					),
					array (
						'key' => 'field_592c460fca346',
						'label' => 'Text',
						'name' => 'home-bottom-banner-text',
						'type' => 'textarea',
						'required' => 1,
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => 100,
						'rows' => '',
						'formatting' => 'br',
					),
					array (
						'key' => 'field_592c2dc70a000',
						'label' => 'Link',
						'name' => 'home-bottom-banner-link',
						'type' => 'text',
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_592c2dc70a001',
						'label' => 'Open in new tab?',
						'name' => 'home-bottom-banner-link-newtab',
						'type' => 'true_false',
						'message' => 'Yes',
						'default_value' => 0,
					),
				),
				'row_min' => 3,
				'row_limit' => 3,
				'layout' => 'row',
				'button_label' => 'Add banner',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'featured_image',
			),
		),
		'menu_order' => 0,
	));
}
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_pages',
		'title' => 'Pages',
		'fields' => array (
			array (
				'key' => 'field_5953bb9dafe80',
				'label' => 'First paragraph',
				'name' => 'post-first-paragraph',
				'type' => 'wysiwyg',
				'default_value' => '',
				// 'toolbar' => 'basic',
				'media_upload' => 'no',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_press',
		'title' => 'Press',
		'fields' => array (
			array (
				'key' => 'field_5953daffd4e44',
				'label' => 'URL',
				'name' => 'press-url',
				'type' => 'text',
				'required' => 1,
				'default_value' => '',
				'placeholder' => 'http://',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5953db28d4e45',
				'label' => 'Excerpt',
				'name' => 'press-excerpt',
				'type' => 'textarea',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'br',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'press',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'excerpt',
			),
		),
		'menu_order' => 0,
	));
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_cake-category',
		'title' => 'Cake Category',
		'fields' => array (
			array (
				'key' => 'field_59540601f6edd',
				'label' => 'Book Your Tasting Button',
				'name' => 'cake-category-customizeable',
				'type' => 'true_false',
				'message' => 'Display "book your tasting" button for each cake',
				'default_value' => 0,
			),
			array (
				'key' => 'field_59540601f6ed1',
				'label' => 'Flavor Options Button',
				'name' => 'cake-category-flavor-options-button',
				'type' => 'true_false',
				'message' => 'Display "Flavor Options" button for each cake',
				'default_value' => 1,
			),
			array (
				'key' => 'field_59540601f6ed2',
				'label' => 'Customize Your Cake Button',
				'name' => 'cake-category-customize-button',
				'type' => 'true_false',
				'message' => 'Display "Customize Your Cake" button for each cake',
				'default_value' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'cake_category',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_sugar-hills-cafe',
		'title' => 'Sugar Hills Café',
		'fields' => array (
			array (
				'key' => 'field_5978cf3f31bbb',
				'label' => 'Image for home page',
				'name' => 'sh-cafe-image',
				'type' => 'image',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-sh-cafe.php',
					'order_no' => 0,
					'group_no' => 0,
				),
				array (
					'param' => 'page_parent',
					'operator' => '!=',
					'value' => '136',
					'order_no' => 1,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
