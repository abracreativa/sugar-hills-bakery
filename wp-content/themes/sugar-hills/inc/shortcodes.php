<?php
/**
* Gallery */

function shortcode_gallery($atts, $content = null){

  extract( shortcode_atts( array(
    'ids' => '',
  ), $atts ) );

  $array_id = explode(",", $ids);
  $return_html = '';
  $cont = 0;

   if ( !empty($array_id) ) {

      $return_html .= '<div class="sugar-hills-gallery photoswipe-container">';

      foreach ( $array_id as $id ) {
        $attachment = sugar_hills_wp_get_attachment($id);
        $caption = $attachment['caption'];
        $src_thumb  = wp_get_attachment_image_src( $id, 'sh-thumb-large' );
        $src_full   = wp_get_attachment_image_src( $id, 'high-res' );

        // Open figure container
        $return_html .= '<figure class="sugar-hills-gallery-item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">';

        // Image link
        $return_html .= sprintf(
          '<a href="%s" itemprop="contentUrl" data-size="%sx%s"><img src="%s" itemprop="thumbnail" alt="%s" /></a>',
          $src_full[0],
          $src_full[1],
          $src_full[2],
          $src_thumb[0],
          $caption ) ;

        // Figure caption
        if( $caption )
          $return_html .= sprintf( '<figcaption itemprop="caption description">%s</figcaption>', $caption );

        // Close figure container
        $return_html .= '</figure>';

        // $return_html .= '<li class="gallery-item post_gallery_item post_gallery_item_'.$paridad.' post_gallery_item_'.$cont.'"><a href="'. $src_full[0] .'" data-fancybox-group="gallery" title="'.$caption.'"><img src="' . $src_thumb[0] . '"  alt="'.$caption.'" /></a></li>';
      }

      $return_html .= '</div>';
   }

   return $return_html;
}

remove_shortcode( 'gallery' );
add_shortcode( 'gallery', 'shortcode_gallery' );

/**
* Text In Columns */
function shortcode_text_columns($atts, $content = null){

  if( $content )
    return sprintf( '<div class="sugar-hills-text-columns">%s</div>', $content );

}
add_shortcode( 'text-columns', 'shortcode_text_columns' );


function the_content_filter($content) {
	// array of custom shortcodes requiring the fix
	$block = join("|",array("text-columns"));
	// opening tag
	$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);

	// closing tag
	$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
	return $rep;
}
add_filter("the_content", "the_content_filter");
