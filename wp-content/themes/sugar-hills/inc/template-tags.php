<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Sugar_Hills_Bakery
 */

if ( ! function_exists( 'sugar_hills_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function sugar_hills_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'sugar-hills' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'sugar-hills' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'sugar_hills_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function sugar_hills_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'sugar-hills' ) );
		if ( $categories_list && sugar_hills_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'sugar-hills' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'sugar-hills' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'sugar-hills' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'sugar-hills' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'sugar-hills' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function sugar_hills_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'sugar_hills_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'sugar_hills_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so sugar_hills_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so sugar_hills_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in sugar_hills_categorized_blog.
 */
function sugar_hills_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'sugar_hills_categories' );
}
add_action( 'edit_category', 'sugar_hills_category_transient_flusher' );
add_action( 'save_post',     'sugar_hills_category_transient_flusher' );

/*
** Incluir archivo SVG de ícono por nombre
*/
function sugar_hills_get_svg( $name ){
	return sugar_hills_clean_svg( TEMPLATEPATH . sprintf( '/svg/%s.svg', $name) );
}

/*
** Limpiar SVG
*/
function sugar_hills_clean_svg( $path ){

	if( !file_exists( $path ) ){
		// Maybe it's an url
		$wp_upload_path = wp_upload_dir();
		$path = ABSPATH . str_replace(  get_site_url() , '', $path);
	}

	if( !$path || !file_exists( $path )){
		return '';
	}

	// Get full SVG code
	$svg_contents = file_get_contents( $path );

	// Load DOM elements
	$doc = new DOMDocument();
	$doc->loadXML($svg_contents);

	// Get SVG node
	$svg = $doc->getElementsByTagName('svg');

	// Return
	return @$svg->item(0)->C14N();
}

/*
** Título de archivos
*/
function sugar_hills_the_archive_title( $before = '', $after = '', $hierarchical_title_of_parent = false ){
	if ( is_category() ) {
		/* translators: Category archive title. 1: Category name */
		$title = sprintf( __( '%s' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		/* translators: Tag archive title. 1: Tag name */
		$title = sprintf( __( '%s' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		/* translators: Author archive title. 1: Author name */
		$title = sprintf( __( 'Articles by %s' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		/* translators: Yearly archive title. 1: Year */
		$title = sprintf( __( 'Year: %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
	} elseif ( is_month() ) {
		/* translators: Monthly archive title. 1: Month name and year */
		$title = sprintf( __( 'Month: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
	} elseif ( is_day() ) {
		/* translators: Daily archive title. 1: Date */
		$title = sprintf( __( 'Day: %s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title' );
		}
	} elseif ( is_post_type_archive() ) {
		/* translators: Post type archive title. 1: Post type name */
		$title = sprintf( __( '%s' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		if( $hierarchical_title_of_parent ){
			// This parameter means that if we are at the last level
			// of a taxonomy, we display the title of the parent term!
			$this_term = get_term_by( 'slug', get_query_var( 'term' ), $tax->name );
			$term_children = get_term_children( $this_term->term_id, $tax->name );

			if( !$term_children ){
				$parent_term = get_term_by( 'id', $this_term->parent, $tax->name );
				if( $parent_term && !is_wp_error($parent_term) ){
					$title = $parent_term->name;
				} else {
					$title = $this_term->name;
				}
			} else {
				$title = $this_term->name;
			}

			/* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( __( '%s' ), $title );
		} else {

			/* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( __( '%s' ), single_term_title( '', false ) );
		}
	} else {
		$title = __( 'Archives' );
	}

	// show title
	echo $before . $title . $after;
}

/**
 * BreadCrumbs
 */
function sugar_hills_breadcrumbs() {
  // -------------------------------------------------
  // Genera un camino de migas de la página actual
  // -------------------------------------------------
  /* === OPTIONS === */
	$show_home 				= true;
  $text['home']     = 'Home'; // text for the 'Home' link
  $text['category'] = '%s'; // text for a category page
  $text['search']   = 'Search results: "%s"'; // text for a search results page
  $text['tag']      = 'Tag: "%s"'; // text for a tag page
  $text['author']   = 'Articles by %s'; // text for an author page
  $text['404']      = 'Error 404'; // text for the 404 page

  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter   = '<span class="delimiter"> &gt;&gt;</span> '; // delimiter between crumbs
  $before      = '<span class="breadcrumb current">'; // tag before the current crumb
  $after       = '</span>'; // tag after the current crumb
  /* === END OF OPTIONS === */

  global $post;
  $homeLink = get_bloginfo('url');
  $linkBefore = '<span class="breadcrumb">';
  $linkAfter = '</span>';
  $linkAttr = '';
  $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

  if (is_home() || is_front_page()) {

    if ($showOnHome == 1) echo '<div id="crumbs" class="wrap-breadcrumbs"><a href="' . $homeLink . '">' . $text['home'] . '</a></div>';

  } else {

    echo '<div id="crumbs" class="wrap-breadcrumbs">';
    echo $show_home ? sprintf($link, $homeLink, $text['home']) . $delimiter : '';

    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) {
        $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
        $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
        echo $cats;
      }
      echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

    } elseif ( is_search() ) {
      echo $before . sprintf($text['search'], get_search_query()) . $after;

    } elseif ( is_day() ) {
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
      echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
      echo $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
      echo $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
				// Sin categoría para custom post types?
        $slug = $post_type->rewrite;
				// Show custom post type
        printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->name);
				echo $delimiter;
				// Show tax
				$taxonomy = get_object_taxonomies($post);
				$cat = get_the_terms($post->ID, $taxonomy[0]);// $cat = $cat[0];
				if( $cat ){
					// Tree
					foreach ($cat as $_term) {
						// Loop until one of them has no children
						// this sould be the last item
						if( !get_term_children( $_term->term_id, $taxonomy[0] ) ){
							$cat = $_term;
							break;
						}
					}
	        $cats = get_category_parents($cat, TRUE, $delimiter);
					$cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
					$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
					echo $cats . $delimiter;
				}
				// Show current
        if ($showCurrent == 1) echo  $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat;
				// Tree
				foreach ($cat as $_term) {
					// Loop until one of them has no children
					// this sould be the last item
					if( !get_term_children( $_term->term_id, 'category' ) ){
						$cat = $_term;
						break;
					}
				}
        $cats = get_category_parents($cat, TRUE, $delimiter);
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
        $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
        $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }

    } elseif ( is_tax() ) {
			$the_tax = get_query_var( 'taxonomy' );
			$post_type = get_post_type_object(sugar_hills_get_post_types_by_taxonomy($the_tax));
			// Sin categoría para custom post types?
			if( $post_type ){
				$slug = $post_type->rewrite;
				// Show custom post type
				printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->name);
				echo $delimiter;
			}
			$term = get_term_by( 'slug', get_query_var( 'term' ), $the_tax );
			if( $term && $term->parent ):
				$cats = @get_category_parents($term->parent, TRUE, $delimiter);
				if( $cats ){
					if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
					$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
					$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
					echo $cats;
				}
			endif;
			echo $before . sprintf($text['category'], $term->name) . $after;

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->name . $after;

    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      $cats = get_category_parents($cat, TRUE, $delimiter);
      $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
      $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
      echo $cats;
      printf($link, get_permalink($parent), $parent->post_title);
      if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;

    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo $delimiter;
      }
      if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

    } elseif ( is_author() ) {
      global $author;
      $userdata = get_userdata($author);
      echo $before . sprintf($text['author'], $userdata->display_name) . $after;

    } elseif ( is_404() ) {
      echo $before . $text['404'] . $after;
    }

    if ( get_query_var('paged') ) {
      if ( is_category() || is_archive() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Página') . ' ' . get_query_var('paged');
      if ( is_category() || is_archive() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }

    echo '</div>';

  }

}

/**
 * Archive Nav: Navigation for cakes and pastries
 */
function sugar_hills_archive_nav(){
	// List of terms
	$terms = false;
	// Current view
	$this_tax = get_query_var( 'taxonomy' );
	$this_term = get_term_by( 'slug', get_query_var( 'term' ), $this_tax );

	if( is_post_type_archive() ){
		// Es la página principal de archivo
		// Obtengo las taxonomías principales para listar
		$taxonomies = get_taxonomies( array(
			// get_post_type() funciona dentro de la página de archivo
			'object_type' => array( get_post_type() )
		) );

		$terms = get_terms( array(
			'taxonomy' => $taxonomies,
			'hierarchical' => false,
			'parent' => 0,
		) );

	} elseif( is_tax() ){
		// Es una página de categoría
		// Esta categoría tiene hijos?
		if( $this_term ){
			$term_children = get_term_children( $this_term->term_id, $this_tax );

			if( $term_children ){
				// Como la categoría tiene hijos, los mostramos
				$terms = get_terms( array(
					'taxonomy' => $this_tax,
					'parent' => $this_term->term_id,
					'hierarchical' => false,
				) );

			} else {
				// Obtenemos los hijos de esta categoría
				$terms = get_terms( array(
					'taxonomy' => $this_tax,
					'parent' => $this_term->parent,
					'hierarchical' => false,
				) );
			}
		}
	}

	// Show nav ?
	if( $terms ){
		echo '<nav class="sugar-hills-archive-nav"><ul>';
		// Loop and show terms
		foreach ($terms as $term) {
			$active = '';
			if( $this_term && !is_wp_error($this_term) ){
				$active = $this_term->term_id == $term->term_id ? 'active' : '';
			}
			echo sprintf( '<li><a href="%s" class="%s">%s</a></li>', get_term_link($term), $active, $term->name );
		}
		echo '</ul></nav>';
	}
}

/**
 * Get post type by taxonomy
 */
function sugar_hills_get_post_types_by_taxonomy( $tax = 'category' ){
    $out = array();
    $post_types = get_post_types();
    foreach( $post_types as $post_type ){
        $taxonomies = get_object_taxonomies( $post_type );
        if( in_array( $tax, $taxonomies ) ){
            return $post_type;
        }
    }
}

/**
 * Get config
 */
function sugar_hills_config( $key = false ){
  $settings = (array) get_option( 'sugar-hills-config' );
	if( $key ){
		if( isset($settings[$key]) ){
			return $settings[$key];
		} else {
			return false;
		}
	} else {
		return $settings;
	}
}

/**
 * Get attachment by id
 */
function sugar_hills_wp_get_attachment( $attachment_id ) {

  $attachment = get_post( $attachment_id );
  return array(
    // 'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
    'caption' => $attachment->post_excerpt,
    'description' => $attachment->post_content,
    // 'href' => get_permalink( $attachment->ID ),
    'src' => $attachment->guid,
    'title' => $attachment->post_title
  );
}

/**
 * Custom the_post_thumbnail();
 */
function sugar_hills_the_post_thumbnail( $display_size = 'thumbnail', $link_high_res = true ){
	// Variables
  global $post;
	$return_html = '';

	// No thumbnail?
	if( !has_post_thumbnail( $post ) ){
		return false;
	}

	$return_html .= '<div class="photoswipe-container">';

	// Get attachment
	$attachment_id = get_post_thumbnail_id( $post->ID );
	$attachment = sugar_hills_wp_get_attachment( $attachment_id );
	$caption = $attachment['caption'];
	$src_thumb  = wp_get_attachment_image_src( $attachment_id, $display_size );
	$src_full   = wp_get_attachment_image_src( $attachment_id, 'high-res' );

	// Open figure container
	$return_html .= '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">';

	// Image link
	if( $link_high_res ){
		$return_html .= sprintf(
			'<a href="%s" itemprop="contentUrl" data-size="%sx%s"><img src="%s" itemprop="thumbnail" alt="%s" /></a>',
			$src_full[0],
			$src_full[1],
			$src_full[2],
			$src_thumb[0],
			$caption ) ;
	} else {
		$return_html .= sprintf(
			'<img src="%s" itemprop="thumbnail" alt="%s" />',
			$src_thumb[0],
			$caption ) ;
	}

	// Figure caption
	if( $caption )
		$return_html .= sprintf( '<figcaption itemprop="caption description">%s</figcaption>', $caption );

	// Close figure container
	$return_html .= '</figure>';
	$return_html .= '</div>';

	// Show it
	echo $return_html;
}

/**
 * Get the URL of "something"
 */
function sugar_hills_get_permalink( $slug, $type = 'page', $tax = false ){

	$url = false;

	if( $type == 'auto' ){
		$url = get_permalink( get_page_by_path( $slug ) );
	}
	if( $type == 'category' ){
		// Get the ID of a given category
		$category_id = get_cat_ID( $slug );
		// Get the URL of this category
		$url = get_category_link( $category_id );
	}
	if( $type == 'term' && $tax ){
		$url = get_term_link( get_term_by( 'slug', $slug, $tax ) );
	}

	return $url;
}

/**
 * Get an image URL assigned to a term
 */
function sugar_hills_get_term_image_url( $term_obj, $taxonomy ){

	// Attempt to get image
	$image_url = false;
	$image_array = get_field( 'category-featured-image', $term_obj );
	if( $image_array ){
		// There is an image assigned
		$image_url = $image_array['sizes']['wide-header'];
	} else {
		// This term has no imagen assigned.
		// Check parents
		$break = false;
		while ( !$break ) {
			$term_id = $term_obj->parent;
			if( $term_id ){
				$term_obj = get_term_by( 'id', $term_id, $taxonomy );
				$image_array = get_field( 'category-featured-image', $term_obj );
				if( $image_array ){
					// This term has image
					$image_url = $image_array['sizes']['wide-header'];
				}
			} else {
				// No more parents
				$break = true;
			}
		}
	}

	return $image_url;
}

/**
 * Get the header image
 * Display as CSS background-image
 */
function sugar_hills_header_image(){

	global $post;
	global $wp_query;
	$image_url = '';

	if( is_page() && has_post_thumbnail() ){

		// Pages display the image asigned as featured
		$attachment_id = get_post_thumbnail_id( $post->ID );
		$attachment = sugar_hills_wp_get_attachment( $attachment_id );
		$src_full   = wp_get_attachment_image_src( $attachment_id, 'wide-header' );
		// Image URL
		$image_url	= $src_full[0];

	} elseif( is_tax() ){

		// Taxonomies can have an image asigned (through ACF)
		// If they don't, check their parents
		// If they don't, use random
		// Get tax and term
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		$this_term = get_term_by( 'slug', get_query_var( 'term' ), $tax->name );
		// Attempt to get image
		$image_url = sugar_hills_get_term_image_url( $this_term, $tax->name );

	} elseif( is_single() ) {

		// Singles should use the image of its term
		$taxonomy = get_object_taxonomies( $post );
		$taxonomy = $taxonomy[0];
		// Get all terms, but make sure it's the last child
		$terms = get_the_terms( $post, $taxonomy );
		$this_term = false;
		if( $terms && !is_wp_error( $terms ) ){
			foreach ($terms as $the_term) {
				$term_children = get_term_children( $the_term->term_id, $taxonomy );
				// var_dump($term_children);
				if( !$term_children || is_wp_error($term_children) ){
					// This term has no children. Use it.
					$this_term = $the_term;
				}
			}
		}
		// Attempt to get image
		if( $this_term ){
			$image_url = sugar_hills_get_term_image_url( $this_term, $taxonomy );
		}

	} elseif( is_post_type_archive() ) {

		$_query_post_type = $wp_query->query['post_type'];
		$_image_path = get_template_directory() . '/images/header-archive-' . $_query_post_type . '.jpg';

		if( file_exists( $_image_path ) ){
			$image_url = get_template_directory_uri() . '/images/header-archive-' . $_query_post_type . '.jpg';
		}


	} else {

		// Didn't match any previous case.
		// Use default

	}

	// Display
	if( ! $image_url ){
		$image_url = get_template_directory_uri() . '/images/header-default.jpg';
	}

	// Show image
	echo sprintf('style="background-image:url(%s);"', $image_url);

}
