<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Sugar_Hills_Bakery
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function sugar_hills_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'sugar_hills_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function sugar_hills_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'sugar_hills_pingback_header' );

/**
 * Automatically assign parent categories
 */
function sugar_hills_assign_parent_terms( $post_id ){
	global $post;

	$taxonomies = get_object_taxonomies( get_post_type( $post_id ) );

	foreach ($taxonomies as $taxonomy) {
		// get all assigned terms
		$terms = wp_get_post_terms($post_id, $taxonomy );
		if( !$terms || is_wp_error($terms) ){
			return $post_id;
		}
		foreach($terms as $term){
			while($term->parent != 0 && !has_term( $term->parent, $taxonomy, $post )){
		  	// move upward until we get to 0 level terms
		  	wp_set_object_terms($post_id, array($term->parent), $taxonomy, true);
		  	$term = get_term($term->parent, $taxonomy);
		  }
		}
	}

	return $post_id;
}
add_action('save_post', 'sugar_hills_assign_parent_terms');

/**
 * Remove posts menu
 */
function sugar_hills_post_remove(){
  remove_menu_page('edit.php');
}
add_action('admin_menu', 'sugar_hills_post_remove');

/**
 * Login Screen
 */
function sugar_hills_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'sugar_hills_logo_url' );

function sugar_hills_logo_url_title() {
    return get_bloginfo('name');
}
add_filter( 'login_headertitle', 'sugar_hills_logo_url_title' );
function sugar_hills_login_logo() {
  $custom_logo = get_template_directory_uri() . '/svg/logo.svg';
  ?>
  <style type="text/css">
      #login h1 a, .login h1 a {
          background-image: url(<?php echo $custom_logo; ?>);
          padding-bottom: 0;
					margin-bottom: 0;
          background-size: 80% 80%;
          background-position: center center;
          width: auto;
					padding: 0;
					height: 160px;
					/*background-color: #30a092;*/
      }
			#loginform{
				margin-top: 0;
			}
			#login #login_error, #login .message{
				border-left: 0;
			}
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'sugar_hills_login_logo' );

/**
 * Add photoswipe's HTML to footer
 */
function sugar_hills_photoswipe_html() {
	echo '<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"><div class="pswp__bg"></div><div class="pswp__scroll-wrap"><div class="pswp__container"> <div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"> <div class="pswp__top-bar"> <div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Cerrar (Esc)"></button> <button class="pswp__button pswp__button--share" title="Compartir"></button> <button class="pswp__button pswp__button--fs" title="Pantalla completa"></button> <button class="pswp__button pswp__button--zoom" title="Zoom +/-"></button> <div class="pswp__preloader"> <div class="pswp__preloader__icn"> <div class="pswp__preloader__cut"> <div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"> <div class="pswp__share-tooltip"></div></div><button class="pswp__button pswp__button--arrow--left" title="Anterior (flecha izquierda)"> </button> <button class="pswp__button pswp__button--arrow--right" title="Siguiente (flecha derecha)"> </button> <div class="pswp__caption"> <div class="pswp__caption__center"></div></div></div></div></div>';
}
add_action( 'wp_footer', 'sugar_hills_photoswipe_html' );

/**
 * Mailchimp PopPUP
 */
function sugar_hills_mailchimp_popup() {
	echo '<script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">setTimeout(function(){require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us16.list-manage.com","uuid":"103a055ac88ab3a2e86ebe6ad","lid":"fb6f28f558"}) });}, 18000)</script>';
}
add_action( 'wp_footer', 'sugar_hills_mailchimp_popup', 100 );

/**
 * Menú de Sugar Hills Café con el logo
 */
function sugar_hills_logo_menu_cafe( $menu_html ) {

	$pos = strpos($menu_html, 'id="menu-sh-cafe"');

	if ($pos === false) {
    return $menu_html;
	} else {
		$logo = sprintf(
			'<a href="%s" class="sugar-hills-cafe-logo">%s</a>',
			get_permalink( get_page_by_path( 'sugar-hills-cafe' ) ),
			sugar_hills_get_svg('logo-sh-cafe')
		);
    return $logo . $menu_html;
	}
}
add_filter( 'wp_nav_menu', 'sugar_hills_logo_menu_cafe');
