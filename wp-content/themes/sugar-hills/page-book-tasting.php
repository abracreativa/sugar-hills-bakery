<?php
/**
 * Template Name: Book your tasting
 *
 * @package Sugar_Hills_Bakery
 */

 include TEMPLATEPATH . '/_cake_variables.php';
 get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'page' );

				endwhile; // End of the loop.
				?>

				<form class="book-tasting-form" action="<?php echo get_template_directory_uri(); ?>/sendmail/sendmail.php" method="post">
          <?php if( isset($_GET['mailerror']) ):
              echo sprintf( '<p class="sugar-hills-message sh-msg-%s">%s</p>',
              $_GET['mailerror'] == '1' ? 'error' : 'success',
              urldecode($_GET['message'])
            );
           endif; ?>

          <h2><label><input class="required" type="radio" name="Tasting_options[]" data-toggle-item="#venue_packages" value="Venue Packages"> Venue Packages <small class="title-note">(pick this option if you are in contract with a venue to host your event)</small></label> <label for="Tasting_options[]" class="error"></label></h2>
          <div class="radio-toggle-container" id="venue_packages">
            <p>A Consultation for venue package is free of charge and it includes:</p>
            <ul>
              <li>4 cake flavor options of vanilla, chocolate, half &amp; half and marble.</li>
              <li>Vanilla &amp; chocolate buttercream plus four additional choices of buttercream.</li>
            </ul>
            <p><label>What venue is your event at?</label>
            <input class="required" type="text" name="Tasting_options[Venue_Name]" id="Tasting_options[Venue_Name]"></p>
          </div>
          <h2><label><input class="required" type="radio" name="Tasting_options[]" data-toggle-item="#private_ceremonies" value="Private Ceremonies"> Private Ceremonies</label></h2>
          <div class="radio-toggle-container" id="private_ceremonies">
            <p>A Consultation fee of $25 (non-refundable) is required to book this appointment. To request a consultation please fill out the cake flavor &amp; filling options below. You may select up to ​ <strong>2 cake flavors</strong>​ and ​ <strong>6 filling flavors​</strong> for your tasting appointment.</p>

            <h3>Cake Flavors</h3>
            <p><strong>Pick up to 2 cake flavors you would like to taste:</strong>
            <br><label for="Cake_Flavors[]" class="error"></label></p>
            <p class="sugar-hills-text-columns">
              <?php foreach ($cake_flavors as $_cake_option):
                echo sprintf( '<label><input data-max-checked="2" class="required" type="checkbox" name="Cake_Flavors[]" id="Cake_Flavors" value="%1$s"> %1$s</label><br>', $_cake_option );
              endforeach; ?>
            </p>

            <h3>Filling Flavors</h3>
            <p><strong>Pick up to 6 filling flavors you would like to taste:</strong>
              <br><label for="Filling_Flavor[]" class="error"></label></p>
            <?php foreach ($filling_options as $key => $_filling_option):
              echo sprintf( '<h5 class="separator-title">%1$s <small>%2$s</small></h5>', $_filling_option['title'], $_filling_option['description'] );
              echo '<p class="sugar-hills-text-columns">';
              foreach ($_filling_option['options'] as $_fop) {
                echo sprintf( '<label><input data-max-checked="6" class="required" type="checkbox" name="Filling_Flavor[]" id="Filling_Flavor" value="%2$s: %1$s"> %1$s</label><br>', $_fop, $_filling_option['title'] );
              }
              echo '</p>';
            endforeach; ?>

          </div>

          <h2>Tasting Date &amp; Time</h2>
          <?php
          /**
           * 2018-06-01
           * @Lonnie Rodriguez: We're not going to be holding any tastings at this location until we're finished remodeling the tasting room.
           */
          /*<p><strong>To book a tasting at our Algonquin location please call 847-658-8765</strong></p>*/ ?>
          <?php /*<p>Choose your tasting date: <input class="required" type="date" name="Tasting_Date_and_Time[Date]" id="Tasting_Date"></p>
          <p>Choose your tasting time: <select id="Tasting_Time" class="required" name="Tasting_Date_and_Time[Time]">
            <option value=""></option>
            <optgroup class="datetime-weekdays" label="Monday - Friday">
              <option value="12pm - 1pm"> 12pm - 1pm</option>
              <option value="1pm - 2pm"> 1pm - 2pm</option>
              <option value="2pm - 3pm"> 2pm - 3pm</option>
              <option value="3pm - 4pm"> 3pm - 4pm</option>
              <option value="4pm - 5pm"> 4pm - 5</option>
            </optgroup>
            <optgroup class="datetime-weekend" label="Saturday">
              <option value="9am - 10am"> 9am - 10am</option>
              <option value="10am - 11am"> 10am - 11am</option>
              <option value="11am - 12pm"> 11am - 12pm</option>
              <option value="12pm - 1pm"> 12pm - 1pm</option>
              <option value="1pm - 2pm"> 1pm - 2pm</option>
              <option value="2pm - 3pm"> 2pm - 3pm</option>
            </optgroup>
          </select></p>*/ ?>
          <p>Please select a date on the calendar</p>
          <div class="form-text-fields" id="sh-calendar-root"></div>

          <div class="form-text-fields">
            <h2>Contact Information</h2>
            <p><label>Full Name</label> <input class="required" type="text" name="Contact_information[Full_Name]" id="Contact_information[Full_Name]"></p>
            <p><label>Email</label> <input class="required" type="email" name="Contact_information[Email]" id="Contact_information[Email]"></p>
            <p><label>Telephone</label> <input class="required" type="tel" name="Contact_information[Telephone]" id="Contact_information[Telephone]"></p>
            <p>
              <div class="g-recaptcha" data-sitekey="6Lc-VycUAAAAABpVDdZXCNSBnxp2VtdSP42c69Us"></div>
            </p>
          </div>

					<p>
            <input type="hidden" name="Form_submit" id="Form_submit" value="1">
            <input type="hidden" name="Return_To" id="Return_To" value="<?php the_permalink(); ?>">
            <input type="hidden" name="Contact_information[Subject]" id="Contact_information[Subject]" value="Book your tasting">
            <input type="hidden" name="Form" id="Form" value="e-mail-book-tasting">
  					<button type="submit" class="sugar-hills-button"><span>Submit Your Appointment Request</span></button>
          </p>
				</form>

			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
