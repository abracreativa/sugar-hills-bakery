<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sugar_Hills_Bakery
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<?php sugar_hills_breadcrumbs(); ?>
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			if( get_field( 'post-first-paragraph' ) ){
				echo sprintf( '<div class="sugar-hills-first-paragraph">%s</div>', wpautop(get_field('post-first-paragraph')) );
			}
			the_content(); ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
