<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Sugar_Hills_Bakery
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<p><span class="footer-copyright">&copy; <?php echo date('Y') . ' ' . get_bloginfo('name'); ?>. All Rights Reserved.</span><span class="footer-social-nav"><?php
				$config = sugar_hills_config();
				if( isset($config['facebook']) && $config['facebook'] ){
					echo sprintf( '<a target="_blank" href="%s">%s</a> ', $config['facebook'], sugar_hills_get_svg('facebook') );
				}
				if( isset($config['instagram']) && $config['instagram'] ){
					echo sprintf( '<a target="_blank" href="%s">%s</a> ', $config['instagram'], sugar_hills_get_svg('instagram') );
				}
			?></span></p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->
<button	class="back-to-top"><?php echo sugar_hills_get_svg('up-arrow'); ?></button>

<?php wp_footer(); ?>

</body>
</html>
