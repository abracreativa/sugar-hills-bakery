<?php
/**
 * Sugar Hills Bakery functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Sugar_Hills_Bakery
 */

if ( ! function_exists( 'sugar_hills_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sugar_hills_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Sugar Hills Bakery, use a find and replace
	 * to change 'sugar-hills' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'sugar-hills', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'sugar-hills' ),
	) );
	register_nav_menus( array(
		'menu-sh-cafe' => esc_html__( 'Sugar Hills Café', 'sugar-hills' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'gallery',
		'caption',
	) );

	/*
	 * Image Sizes
	 */
	add_image_size( 'high-res', 1280, 1080, true );
	add_image_size( 'wide-header', 1440, 600, true );
	add_image_size( 'sh-thumb', 300, 240, true );
	add_image_size( 'sh-thumb-large', 475, 400, true );

}
endif;
add_action( 'after_setup_theme', 'sugar_hills_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sugar_hills_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sugar_hills_content_width', 640 );
}
add_action( 'after_setup_theme', 'sugar_hills_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
// function sugar_hills_widgets_init() {
// 	register_sidebar( array(
// 		'name'          => esc_html__( 'Sidebar', 'sugar-hills' ),
// 		'id'            => 'sidebar-1',
// 		'description'   => esc_html__( 'Add widgets here.', 'sugar-hills' ),
// 		'before_widget' => '<section id="%1$s" class="widget %2$s">',
// 		'after_widget'  => '</section>',
// 		'before_title'  => '<h2 class="widget-title">',
// 		'after_title'   => '</h2>',
// 	) );
// }
// add_action( 'widgets_init', 'sugar_hills_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sugar_hills_scripts() {
	$v = date('Ym');
	wp_enqueue_style( 'goog-fonts', 'https://fonts.googleapis.com/css?family=Prata|Roboto+Condensed:400,400i,700' );
	wp_enqueue_style( 'photoswipe-css', get_template_directory_uri() . '/js/photoswipe/photoswipe.css' );
	wp_enqueue_style( 'photoswipe-css-ui', get_template_directory_uri() . '/js/photoswipe/default-skin/default-skin.css' );
	wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/js/owl-carousel/assets/owl.carousel.min.css' );
	wp_enqueue_style( 'sugar-hills-style', get_stylesheet_uri() );

	wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBnoCnsF5tvoavJHHVZho2DNmMAmPGki_w', array(), $v, true );
	wp_enqueue_script( 'google-recaptcha', 'https://www.google.com/recaptcha/api.js', array(), $v, true );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'imagesloaded' );
	wp_enqueue_script( 'jquery-ui-datepicker' );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array(), $v, true );
	wp_enqueue_script( 'photoswipe-js', get_template_directory_uri() . '/js/photoswipe/photoswipe.min.js', array('jquery'), $v, true );
	wp_enqueue_script( 'photoswipe-js-ui', get_template_directory_uri() . '/js/photoswipe/photoswipe-ui-default.min.js', array('jquery'), $v, true );
	wp_enqueue_script( 'sugar-hills-gallery', get_template_directory_uri() . '/js/gallery.js', array(), $v, true );
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.min.js', array('jquery'), $v, true );
	wp_enqueue_script( 'bricklayer', get_template_directory_uri() . '/js/bricklayer.min.js', array('jquery'), $v, true );
	wp_enqueue_script( 'validator', get_template_directory_uri() . '/js/jquery.validate.js', array('jquery'), $v, true );
	wp_enqueue_script( 'sugar-hills-main', get_template_directory_uri() . '/js/main.js', array('jquery', 'imagesloaded', 'jquery-ui-datepicker'), $v, true );
	wp_enqueue_script( 'sugar-hills-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), $v, true );
}
add_action( 'wp_enqueue_scripts', 'sugar_hills_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Plugins
 */
require get_template_directory() . '/plugins/advanced-custom-fields/acf.php';
require get_template_directory() . '/plugins/acf-repeater/acf-repeater.php';

/*
 * Permitir SVG
 */
require get_template_directory() . '/plugins/svg-upload/svg-upload.php';

/**
 * Admin Menu
 */
require get_template_directory() . '/admin/menus.php';

/**
 * Custom fields
 */
require get_template_directory() . '/inc/custom-fields.php';

/**
 * Custom post types
 */
require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Email Functions
 */
require get_template_directory() . '/sendmail/inc-emails.php';
