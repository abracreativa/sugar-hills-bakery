<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sugar_Hills_Bakery
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php
				if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
							sugar_hills_breadcrumbs();
							sugar_hills_the_archive_title( '<h1 class="page-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</header><!-- .page-header -->

					<div class="sugar-hills-two-column-grid">

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post(); ?>

							<article class="sugar-hills-two-column-grid-item">
								<a href="<?php the_field('press-url'); ?>" target="_blank">

									<figure class="sugar-hills-two-column-grid-img">
										<?php the_post_thumbnail( 'sh-thumb' ); ?>
									</figure>
									<div class="sugar-hills-two-column-grid-text">
										<h1><?php echo sugar_hills_get_svg('dingbat'); the_title(); ?></h1>
										<p>
											<?php the_field( 'press-excerpt' ); ?>
										</p>
									</div>

								</a>
							</article>

						<?php
						endwhile; ?>

					</div>

				<?php
					the_posts_navigation();

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif; ?>

			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
