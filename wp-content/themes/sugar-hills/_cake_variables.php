<?php
/**
* @package Sugar_Hills_Bakery
*/

//Cake Flavors
$cake_flavors = array(
 'Vanilla',
 'Chocolate',
 'Walnut Spice',
 'Chocolate-Vanilla Combo',
 'Carrot',
 'Red Velvet',
 'Marble',
);


// Filling Options
$filling_options = array(

array(
  'title' => 'Buttercream',
  'description' => '(from $3.99 per person)',
  'options' => array(
    'Vanilla',
    'Chocolate',
    'Peppermint',
    'Strawberry',
    'Raspberry',
    'Passion Fruit',
    'Mango',
    'Orange',
    'Lemon',
    'Banana',
    'Coconut',
    'Mocha',
    'Peanut Butter',
    'Hazelnut',
    'Almond',
    'Caramel',
    'Salted Caramel',
  ),
),

array(
  'title' => 'Mousse',
  'description' => '(from $4.50 per person)',
  'options' => array(
    'Vanilla',
    'Chocolate',
    'Raspberry',
    'Strawberry',
    'Passion Fruit',
    'Mango',
    'Orange',
    'Lemon',
    'Banana',
    'Coconut',
    'Mocha',
    'Peanut Butter',
    'Hazelnut',
    'Almond',
  ),
),

array(
  'title' => 'Custard',
  'description' => '(from $3.99 per person)',
  'options' => array(
    'Vanilla',
    'Chocolate',
    'Strawberry',
    'Raspberry',
    'Passion Fruit',
    'Mango',
    'Orange',
    'Lemon',
    'Banana',
    'Coconut',
  ),
),

array(
  'title' => 'Specialty Mousse',
  'description' => '(from $5.99 per person)',
  'options' => array(
    'Irish Coffee Cream',
    'White Chocolate with Creme de Cacao & Chocolate Chips',
    'Stracciatella',
    'Pear with pieces of pears',
  ),
),

);


// Flavor options - Every Day Cakes
$everyday_cakes = array(
	'Chocolate Roses',
	'Ambrosia',
	'Raspberry Snow',
	'Nutcracker',
	'Double Chocolate Mousse',
	'Triple Berry',
	'Pear Extravaganza',
	'Mango Madness',
	'Stracciatella',
	'Carrot Cake',
	'Tiramisu',
	'Black Forest',
	'Red Velvet',
	'Caramel Macchiato',
	'German Chocolate',
	'Irish Coffee Cream',
	'Neopolitan',
	'White Chocolate Strawberry',
	'Naked Strawberry Lemonade',
	'Nutella',
	'Vanilla Carousel',
	'Salted Caramel',
	'Raspberry Brandy',
	'Rocky Road',
	'Mocha',
	'Pink Roses',
	'Fresh Strawberry Shortcake',
);

//Cake Options
$cake_options = array(
 'Carrot',
 'Chocolate',
 'Lemon',
 'Marble',
 'Red Velvet',
 'Walnut Spice',
 'Vanilla',
);

// Cake Sizes
$cake_sizes = array(
 '6” serves 8-10ppl',
 '8” serves 12-15ppl',
 '10” serves 20-25ppl',
 '12” serves 35-40ppl',
 '14” serves 45-50 ppl',
 'Half sheet serves 55-60ppl',
);
$cake_sizes_2 = array(
  '2 Tiers serves 35+',
  '3 Tiers serves 60+',
  '4 Tiers serves 120+',
  '5 Tiers serves 200+',
  '6 Tiers serves 340+',
);

// Delivery Options
$delivery_options = array(
  'I want my cake delivered (delivery fee will apply)',
  'I will pick up my cake at Sugar Hills Bakery & Cafe (3235 W Addison St. Chicago, IL 60618)',
  'I will pick up my cake at Sugar Hills Algonquin (644 S Main St. Algonquin, IL 60102)',
);
