<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sugar_Hills_Bakery
 */

 include get_template_directory() . '/archive-cakes.php';
