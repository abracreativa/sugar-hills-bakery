<?php

require_once  '../../../../wp-load.php';
require_once 'inc-emails.php';
require_once 'reCaptcha-autoload.php';
require_once 'Mailchimp.php';

// tu clave secreta
$secret = "6Lc-VycUAAAAAPTRn2VtcCJUfRW2CdoFfQYaR6HH";

// respuesta vacía
$response = null;

// comprueba la clave secreta
$recaptcha = new \ReCaptcha\ReCaptcha($secret);

// si se detecta la respuesta como enviada
if ($_POST["g-recaptcha-response"]) {
  $response = $recaptcha->verify(
    $_POST["g-recaptcha-response"],
    $_SERVER["REMOTE_ADDR"]
  );
}
if ($response != null && $response->isSuccess()) {

  require 'PHPMailerAutoload.php';
  //-----------------------------
  // :: Datos de quien envía
  //-----------------------------
  $the_email    = $_POST['Contact_information']['Email'];
  $the_name     = $_POST['Contact_information']['Full_Name'];
  $the_subject  = $_POST['Contact_information']['Subject'];
  $destinatario = trim( $_POST['Form'] );
  $config       = sugar_hills_config();

  //-----------------------------
  // :: Revisar si se adjuntan archivos
  //-----------------------------
  $attachment = false;

  if( isset($_FILES) && !empty($_FILES) ){

    if ( isset($_FILES['file']) && $_FILES['file']['error'] == UPLOAD_ERR_OK ){
      // Se subió correctamente
      $attachment = array(
        'tmp_name' => $_FILES['file']['tmp_name'],
        'name' => $_FILES['file']['name'],
      );
    }
  }

  //-----------------------------
  // :: Suscribir a MailChimp
  //-----------------------------
  if( isset($_POST['Contact_information']['Subscribe_to_newsletter']) && $_POST['Contact_information']['Subscribe_to_newsletter'] == 'Yes' ):

    $MailChimp = new Mailchimp('3ffc07a7ba7f993de37763cc4ae7ac06-us16');
    $result = $MailChimp->call('lists/subscribe', array(
                    'id'                => 'fb6f28f558',
                    'email'             => array('email'=> $the_email),
                    'merge_vars'        => array('FNAME'=> $_POST['Contact_information']['First_Name'], 'LNAME'=> $_POST['Contact_information']['Last_Name']),
                    'double_optin'      => false,
                    'update_existing'   => true,
                    'replace_interests' => false,
                    'send_welcome'      => false,
                ));
    $_POST['Contact_information']['Newsletter'] = 'The user was added to the list.';

  else:

    $_POST['Contact_information']['Newsletter'] = 'The user was *not* added to the list.';

  endif;

  //-----------------------------
  // :: Book your tasting
  //-----------------------------
  if( isset($_POST['sh-calendar-datetime']) && isset($_POST['sh-calendar-pretty-datetime']) ){
    // We came from book your tasting.
    // Set the date we are going to send
    $calendar_pretty_datetime = $_POST['sh-calendar-pretty-datetime'];
    $calendar_datetime        = date('c', strtotime($_POST['sh-calendar-datetime']));
    $calendar_datetime_end    = date('c', strtotime($_POST['sh-calendar-datetime']) + 3600);

    // Unset unused post variables
    unset($_POST['sh-calendar-pretty-datetime']);
    unset($_POST['sh-calendar-datetime']);
    unset($_POST['sh-time']);

    // Set the ones we will use
    $_POST['Tasting_Date_and_Time']['Date'] = $calendar_pretty_datetime;

    // Construct the event details
    $calendar_emails = explode(',' , $config[ $destinatario ]);
    array_push($calendar_emails, $the_email);
    $attendees = array();
    foreach ($calendar_emails as $__email) {
      array_push($attendees, array('email' => trim($__email)));
    }

    $event_details = array(
      'summary' => 'Sugar Hills BYT: ' . $the_name,
      'location' => @$config['address-1'],
      'description' => build_email_html($_POST, array('Return_To', 'Form_submit', 'Form', 'g-recaptcha-response', 'Return_To')),
      'start' => array(
          'dateTime' => $calendar_datetime,
          'timeZone' => 'America/Chicago',
      ),
      'end' => array(
          'dateTime' => $calendar_datetime_end,
          'timeZone' => 'America/Chicago',
      ),
      'attendees' => $attendees,
    );

    $url = apply_filters('sh_calendar_add_event', $event_details);
    
    // Give a message about Google Cal
    // Keep in mind this is used later on to know what message to show the user
    $_POST['Tasting_Date_and_Time']['Google_Calendar'] = 'The appointment was automatically added to Google Calendar. If you own that calendar, you can see the event here: ' . $url;
  }

  //-----------------------------
  // :: Construir HTML de email
  //-----------------------------
  $_POST['Date_Sent'] = date("l j  F Y, h:i:s A");

  //-----------------------------
  // :: Configuración
  //-----------------------------

  $configEmail = array(
    'from_email'    => 'no-reply@sugarhillsbakery.com',
    'from_name'     => get_bloginfo('name'),
    'to'            => explode(',' , $config[ $destinatario ]),
    'bcc'           => array('nahuel@abracreativa.com'),
    'the_email'     => sanitize_element($the_email),
    'the_name'      => sanitize_element($the_name),
    'subject'       => sanitize_element($the_subject),
    'content'       => build_email_html($_POST, array('Return_To', 'Form_submit', 'Form', 'g-recaptcha-response', 'Return_To')),
    'logo_url'      => '',
    'attachment'    => $attachment,
  );

  //-----------------------------
  // :: Enviar email
  //-----------------------------
  if( send_html_email($configEmail) ) {
    $return['error'] = false;
    if( isset($_POST['Tasting_Date_and_Time']['Google_Calendar']) ){
      $return['message'] = 'Your appointment has been confirmed.  Please check your email for a calendar request';
    } else {
      $return['message'] = "Your message was submitted correctly. We will get back to you as soon as possible.";
    }
  } else {
    $return['error'] = true;
    $return['message'] = 'There was an error submitting your request. Please try again later.';
  }

} else {
  $return['error'] = true;
  $return['message'] = 'Unable to validate reCaptcha.';
}

// var_dump($return);
header('location:' . $_POST['Return_To'] . '?mailerror='.intval($return['error']) . '&message=' . urlencode($return['message']) );
