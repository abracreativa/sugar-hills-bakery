<?php
/*
    Sanitize values.
    Replace underscore with spaces and other characters
  */
function sanitize_element($string, $replace_special_chars = true){
  $string = trim($string);

  // Replace elements not allowed in "name" attribute
  // MUST be in this order

  if($replace_special_chars){
    $string = str_replace('__C__', ',', $string);
    $string = str_replace('__B__', '/', $string);
    $string = str_replace('__Q__', '?', $string);
    $string = str_replace('__D__', '-', $string);
    $string = str_replace('__PO__', '(', $string);
    $string = str_replace('__PC__', ')', $string);
    $string = str_replace('_', ' ', $string);
  }

  $string = nl2br(htmlspecialchars($string));

  return $string;
}

/*
  Build the HTML
  */
function build_email_html($__ARR_ELEMS, $__ARR_IGNOREKEYS = Array()){
  $main_html = '';

  // -------------------------------------------------------
  // Loop through elements of POST
  // Array dimensions represent HTML levels
  // Up to 3 levels considered: main_level[second_level][third_level]
  // Keys are used as titles
  // -------------------------------------------------------
  foreach ($__ARR_ELEMS as $_1_key => $_1_post){
  	// First level
  	if(in_array($_1_key, $__ARR_IGNOREKEYS))
  		continue;

  	if(is_array($_1_post)){
  	  // IF element is an array, go to second level
  	  // The key would be the title of this level
  	  $main_html .= '<hr />';
  	  $main_html .= '<h2>' . sanitize_element($_1_key) . '</h2>';

  	  foreach ($_1_post as $_2_key => $_2_post){
  	    // Second Level
  	    if(is_array($_2_post)){
  	      // IF element is array, go to third level
  	      // This level is built with a table

  	      $main_html .= '<table width="100%" border="1" cellpadding="5" cellspacing="0">';
  	      $main_html .= '<tr><th colspan="2">' . sanitize_element($_2_key) . '</th></tr>';
  	      foreach ($_2_post as $_3_key => $_3_post){
  	        // Third and last level.
  	        // Build the cells
  	        $main_html .= '<tr><td width="30%"><strong>' . sanitize_element($_3_key) . ': </strong></td> <td><em>' . sanitize_element($_3_post) . '</em></td></tr>';
  	      }
  	      $main_html .= '</table>';
  	    }else{
  	      // Element is not an array
          if( is_numeric( $_2_key ) ){
            $main_html .= '<p><strong>'. sanitize_element($_2_post) .'</strong></p>';
          } else {
            $main_html .= '<p><strong>' . sanitize_element($_2_key) . ': </strong> <em>' . sanitize_element($_2_post) . '</em></p>';
          }
  	    }
  	  }
  	}else{
  	  // IF element is NOT an array, present it as first level element
  	  // The value would be the title of this level
  	  $main_html .= '<hr />';
  	  $main_html .= '<h2>' . sanitize_element($_1_key) . '</h2>';
  	  $main_html .= '<p>' . sanitize_element($_1_post) . '</p>';
  	}
  }

  return $main_html;

}

/*
Send the email
*/
function send_html_email($arr){
  // Replace the HTML in the Template
  $HTML_TEMPLATE = file_get_contents( getcwd()  . '/sendmail-template.html' );

  // Replace the elements in template
  $HTML_TEMPLATE   = str_replace('%logourl%', $arr['logo_url'], $HTML_TEMPLATE);
  $HTML_TEMPLATE   = str_replace('%subject%', $arr['subject'], $HTML_TEMPLATE);
  $HTML_TEMPLATE   = str_replace('%content%', $arr['content'], $HTML_TEMPLATE);

  // Comenzar el envío del email
  $mail = new PHPMailer;
  $mail->CharSet = 'utf-8';

  // Reply to
  $mail->addReplyTo( $arr['the_email'], $arr['the_name'] );

  // From
  $mail->setFrom( $arr['from_email'], $arr['from_name'] );

  // To
  if( is_array($arr['to']) && !empty($arr['to']) ) {
   foreach($arr['to'] as $toemail){
     $mail->addAddress( $toemail );
   }
  }

  // BCC
  if( is_array($arr['bcc']) && !empty($arr['bcc']) ) {
    foreach($arr['bcc'] as $bbcemail){
      $mail->addBCC( $bbcemail );
    }
  }

  // Attachments
  if( is_array( $arr['attachment'] ) ){
    $mail->AddAttachment( $arr['attachment']['tmp_name'], $arr['attachment']['name'] );
  }

  // Subject
  $mail->Subject = $arr['subject'];

  // Content
  $mail->isHTML(true);
  $mail->Body = $HTML_TEMPLATE;

  return $mail->send();

}
