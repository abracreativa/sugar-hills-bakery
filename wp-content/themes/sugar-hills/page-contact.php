<?php
/**
 * Template Name: Contact Us
 */

$config = sugar_hills_config();
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="content-wrap">

				<?php
				while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="page-header">
							<?php sugar_hills_breadcrumbs(); ?>
							<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<?php
								if( get_field( 'post-first-paragraph' ) ){
									echo sprintf( '<div class="sugar-hills-first-paragraph">%s</div>', wpautop(get_field('post-first-paragraph')) );
								}
								the_content(); ?>

								<div class="row">
									<div class="col-form col-md-4">
										<?php if( isset($_GET['mailerror']) ):
											echo sprintf( '<p class="sugar-hills-message sh-msg-%s">%s</p>',
											$_GET['mailerror'] == '1' ? 'error' : 'success',
											urldecode($_GET['message'])
										);
										 endif; ?>
										<form action="<?php echo get_template_directory_uri(); ?>/sendmail/sendmail.php" method="post">
											<div class="form-text-fields">
												<p><label>First Name</label> <input class="required" type="text" name="Contact_information[First_Name]" id="Contact_information[First_Name]"></p>
												<p><label>Last Name</label> <input class="required" type="text" name="Contact_information[Last_Name]" id="Contact_information[Last_Name]"></p>
												<p><label>Telephone</label> <input class="required" type="tel" name="Contact_information[Telephone]" id="Contact_information[Telephone]"></p>
												<p><label>Email</label> <input class="required" type="email" name="Contact_information[Email]" id="Contact_information[Email]"></p>
												<p><label>Message</label> <textarea name="Contact_information[Message]" id="Contact_information[Message]" class="required"></textarea></p>
												<p><label><input type="checkbox" name="Contact_information[Subscribe_to_newsletter]" value="Yes" checked="checked"> Subscribe to our Newsletter</label></p>
												<p>
													<div class="g-recaptcha" data-sitekey="6Lc-VycUAAAAABpVDdZXCNSBnxp2VtdSP42c69Us"></div>
												</p>
												<p>
							            <input type="hidden" name="Form_submit" id="Form_submit" value="1">
													<input type="hidden" name="Return_To" id="Return_To" value="<?php the_permalink(); ?>">
							            <input type="hidden" name="Form" id="Form" value="e-mail-contact-us">
													<input type="hidden" name="Contact_information[Subject]" id="Contact_information[Subject]" value="Contact Us">
							  					<button type="submit" class="sugar-hills-button"><span>Send</span></button>
							          </p>
											</div>
										</form>
									</div>
									<div class="col-maps col-md-8">
										<?php
											if( isset($config['address-1-lat']) && isset($config['address-1-lat']) ):
										?>
										<div class="row">
											<div class="col-sm-4">
												<h2><?php echo sugar_hills_get_svg('dingbat'); echo $config['address-1-state']; ?></h2>
												<p><?php echo nl2br( $config['address-1'] ); ?></p>
											</div>
											<div class="col-sm-8">
												<div class="g-map" data-lat="<?php echo $config['address-1-lat']; ?>" data-lng="<?php echo $config['address-1-lng']; ?>"></div>
											</div>
										</div>
										<?php endif; ?>
										<?php
											if( isset($config['address-2-lat']) && isset($config['address-2-lat']) ):
										?>
										<div class="row">
											<div class="col-sm-4">
												<h2><?php echo sugar_hills_get_svg('dingbat'); echo $config['address-2-state']; ?></h2>
												<p><?php echo nl2br( $config['address-2'] ); ?></p>
											</div>
											<div class="col-sm-8">
												<div class="g-map" data-lat="<?php echo $config['address-2-lat']; ?>" data-lng="<?php echo $config['address-2-lng']; ?>"></div>
											</div>
										</div>
										<?php endif; ?>
									</div>
								</div>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->

				<?php endwhile; // End of the loop. ?>

			</div><!-- .content-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
